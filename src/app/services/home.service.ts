import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Area } from "../models/area.model";
import { Building } from "../models/building.model";
import { Channel } from "../models/channel.model";
import { Floor } from "../models/floor.model";
import { Measurement } from "../models/measurements/measurement";
import { DataServices } from "./data.service";
import { calendarSearch } from '../models/calendarSearch.model';
import { SensorType } from "../models/measurements/sensorType.model";
import { MeasurementType } from "../models/measurements/measurementType.model";
import { ObjectType } from "../models/measurements/objectType.model";
import { Energy } from "../models/energy.model";
import { DatePipe } from "@angular/common";


@Injectable()
export class HomeService {
  buildings: Array<Building> = [];
  currentFloor!: Floor;
  loadedFloors!: Floor[];
  measurement: Measurement = new Measurement();
  calendarSearch: calendarSearch = new calendarSearch();
  totalFloorConsumtion: number = 0;
  dateValues: {date: string, value: number}[] = [];
  energy = new Energy;
  private datepipe: DatePipe = new DatePipe('en-EU')
  constructor(private api: DataServices){}
  
  async loadBuilding() {
    this.api.getBuildings().subscribe(
      (buildings: any) => {
        if(buildings.length > 0) {
          this.buildings = buildings;
          this.loadFloors(this.buildings[0].id);
        }
      },
      err => {
        console.log(err);
      }
      );
    }
    
    async loadFloors(floorId: string) {
      this.api.getFloors(floorId).subscribe(
        (floors: any) => {
          if(floors !== null) {
            this.loadedFloors = floors;
            this.currentFloor = this.loadedFloors[0]; //
          }
        },
        err => {
          console.log(err);
        }
        );
      }
      
      async loadFloorEnergyConsumption(Floor: Floor, calendarSearch: calendarSearch): Promise<Energy> {
        this.energy = new Energy();
        var dateRange = await this.getDatesBetween(calendarSearch.minDate, calendarSearch.maxDate);
       await dateRange.forEach(async (dt: Date) => {
              var measurement: Measurement = new Measurement();
              measurement.CalendarSearch = calendarSearch;
              measurement.ObjectId = Floor.id;
              measurement.SensorType = SensorType.Energy;
              measurement.MeasurementType = MeasurementType.Sum;
              measurement.ObjectType = ObjectType.Area;
              await this.getMeasurement(measurement, dt);
          });
          var time = (parseInt(calendarSearch.timeInterval[1])  - parseInt(calendarSearch.timeInterval[0])) * dateRange.length;
          this.energy.total = (this.energy.total * time ) / 1000;
          return this.energy;
        }

        async getMeasurement(meas: Measurement, date: Date) {
          (await this.api.getMeasurement(meas)).subscribe(
            (data: any) => {
              if(data != undefined) {
                this.energy.total += data.sum; 
                let formatedDate = this.datepipe.transform(date, 'MMMM d') ?? "";
                this.energy.timeLine.push({date: formatedDate, value: data.sum})
              }
            },
            err => {
              console.log(err);
            });
        }
        
        async getDatesBetween(minDate: Date, maxDate: Date): Promise<Date[]> {
          var dates: Date[] = [];
          var daysBetween = maxDate.getDate() - minDate.getDate();
          var mDate = minDate;
          var initial = true;
          for(var x = 0; x <= daysBetween; x++) {
            var newDate = initial == true ? mDate.setDate(minDate.getDate()) : mDate.setDate(minDate.getDate() + 1);
            dates.push(new Date(newDate));
            initial = false;
          }
          return dates;
        }
      }