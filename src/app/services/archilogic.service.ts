import { Injectable } from "@angular/core";
import { environment as env } from "src/environments/environment";

@Injectable()
export class ArchilogicServices{
    private publishableToken:string;
    private buildingSceneId:string;
    constructor(){
        this.publishableToken = env.enappgyData.publishableToken; 
        this.buildingSceneId = ""; // TODO: Should come from FloorPlanId
    }
    getToken(){
        return this.publishableToken;
    }
    setToken(token:string){
        this.publishableToken = token;
    }
    getBuildingScene(){
        return this.buildingSceneId;
    }
    setBuildingScene(id:string){
        this.buildingSceneId = id;
    }
}