import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataNavigationMenuComponent } from './data-navigation-menu.component';

describe('DataNavigationMenuComponent', () => {
  let component: DataNavigationMenuComponent;
  let fixture: ComponentFixture<DataNavigationMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataNavigationMenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataNavigationMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
