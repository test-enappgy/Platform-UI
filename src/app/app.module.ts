import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DataServices } from './services/data.service';
import { ErrorComponent } from './error/error.component';
import { DataNavigationComponent } from './data-navigation/data-navigation.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { CommonModule } from '@angular/common';
import { AccountService } from './services/account.service';

import { DropDownsModule } from '@progress/kendo-angular-dropdowns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GridModule, PDFModule, ExcelModule, } from '@progress/kendo-angular-grid';
import { ChartsModule } from '@progress/kendo-angular-charts';
import { InputsModule } from "@progress/kendo-angular-inputs";
import 'hammerjs';
import { GaugesModule } from '@progress/kendo-angular-gauges';
import { SliderModule } from "@progress/kendo-angular-inputs";
import { ArchilogicServices } from './services/archilogic.service';
import { DataNavigationMenuComponent } from './data-navigation-menu/data-navigation-menu.component';
import { LoginButtonComponent } from './shared/login-button/login-button.component';
import { SignupButtonComponent } from './shared/signup-button/signup-button.component';
import { LogoutButtonComponent } from './shared/logout-button/logout-button.component';
import { AuthenticationButtonComponent } from './shared/authentication-button/authentication-button.component';
import { AuthNavComponent } from './shared/auth-nav/auth-nav.component';
import { LoadingComponent } from './shared/loading/loading.component';
import { FooterComponent } from './shared/footer/footer.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AuthModule } from '@auth0/auth0-angular';
import { AuthHttpInterceptor } from '@auth0/auth0-angular';
import { environment as env } from 'src/environments/environment';
import { ExternalApiComponent } from './pages/external-api/external-api.component';

import { IntlModule } from "@progress/kendo-angular-intl";
import { DateInputsModule } from "@progress/kendo-angular-dateinputs";
import { ButtonsModule } from "@progress/kendo-angular-buttons";
import { LabelModule } from "@progress/kendo-angular-label";
import {LayoutModule} from "@progress/kendo-angular-layout";
import {PopupModule} from "@progress/kendo-angular-popup";
import { IndicatorsModule } from "@progress/kendo-angular-indicators";


import { CalendarControlComponent } from './shared/calendar-control/calendar-control.component';
import { HomeComponent } from './home/home.component';
import { UtilizationComponent } from './utilization/utilization.component';
import { ColorLegendComponent } from './shared/color-legend/color-legend.component';
import { TotalSavingsComponent } from './home/total-savings/total-savings.component';
import { SavingsOverTimeComponent } from './home/savings-over-time/savings-over-time.component';
import { CurrentVsBaselineConsumptionComponent } from './home/current-vs-baseline-consumption/current-vs-baseline-consumption.component';
import { SavingsCategoriesComponent } from './home/savings-categories/savings-categories.component';
import { InterventionsComponent } from './home/interventions/interventions.component';
import { EnvironmentComponent } from './home/environment/environment.component';
import { LockedServiceComponent } from './locked-service/locked-service.component';
import { LoginComponent } from './login/login.component';
import { MultisenseComponent } from './multisense/multisense.component';
import { SenseMinicardComponent } from './multisense/sense-minicard/sense-minicard.component';

@NgModule({
  declarations: [
    AppComponent,
    ErrorComponent,
    DataNavigationComponent,
    DataNavigationMenuComponent,
    LoginButtonComponent,
    SignupButtonComponent,
    LogoutButtonComponent,
    AuthenticationButtonComponent,
    AuthNavComponent,
    LoadingComponent,
    FooterComponent,
    ProfileComponent,
    ExternalApiComponent,
    CalendarControlComponent,
    HomeComponent,
    UtilizationComponent,
    ColorLegendComponent,
    TotalSavingsComponent,
    SavingsOverTimeComponent,
    CurrentVsBaselineConsumptionComponent,
    SavingsCategoriesComponent,
    InterventionsComponent,
    EnvironmentComponent,
    LockedServiceComponent,
    LoginComponent,
    MultisenseComponent,
    SenseMinicardComponent
  ],
  imports: [
    BrowserModule,
    FontAwesomeModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    ButtonsModule,
    DropDownsModule,
    BrowserAnimationsModule,
    GridModule,
    ChartsModule,
    GaugesModule,
    SliderModule,
    InputsModule,
    PDFModule,
    ExcelModule,
    AuthModule.forRoot({
      domain: env.auth.domain,
      clientId: env.auth.clientId,
      audience: env.auth.audience,
      httpInterceptor: {
        allowedList: [env.enappgyData.protectedMessage],
      },
    }),
    IntlModule,
    LabelModule,
    DateInputsModule,
    LayoutModule,
    IndicatorsModule,
    PopupModule,
    InputsModule,
    BrowserAnimationsModule
  ],
  providers: [
    DataServices, AccountService, ArchilogicServices, 
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthHttpInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  /*constructor(library: FaIconLibrary){
    library.addIconPacks(fas);
    library.addIcons(faSquare, faCheckSquare, faCoffee);
  }*/
}
