import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {
  LoaderType,
  LoaderThemeColor,
  LoaderSize,
} from '@progress/kendo-angular-indicators';
import { calendarSearch } from 'src/app/models/calendarSearch.model';
import { Databucket } from 'src/app/models/databucket.model';
import { Measurement } from 'src/app/models/measurements/measurement';
import { MeasurementType } from 'src/app/models/measurements/measurementType.model';
import { ObjectType } from 'src/app/models/measurements/objectType.model';
import { SensorType } from 'src/app/models/measurements/sensorType.model';
import { AccountService } from 'src/app/services/account.service';
import { DataServices } from 'src/app/services/data.service';
import { DatePipe } from '@angular/common';
import { BucketTimeWindow } from 'src/app/models/measurements/bucket-time-window.model';
import { Observable, Subject, Subscription } from 'rxjs';
import { ViewEncapsulation } from "@angular/core";

@Component({
  selector: 'app-sense-minicard',
  templateUrl: './sense-minicard.component.html',
  styleUrls: ['./sense-minicard.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SenseMinicardComponent implements OnInit {
  public consoleLogTag: string = 'senseMinicard';
  public isDataReady: boolean = true;

  @Input() public isDataSummarized!: boolean;
  @Input() public activateStructureFilter: boolean = false;
  @Input() public index!: number;
  @Input() public id: string = '';
  @Output() public idChange = new EventEmitter<string>();
  @Input() public description = '';
  @Output() public descriptionChange = new EventEmitter<string>();
  @Input() public type: string = '';
  @Input() public sensorType: string = '';
  @Input() public parentId: string = '';
  @Input() public parentDescription: string = '';
  @Input() public databuckets: Databucket[] = [];
  @Input() public treeStructure: any[] = []; //for tree drop down
  @Input() public treeSelectedValue!: any;

  @Input() csEventChild!: Observable<calendarSearch>;
  private csEventsSubscription!: Subscription;

  //Variables related to KendoLoader
  public loaders = [
    {
      type: <LoaderType>'pulsing',
      themeColor: <LoaderThemeColor>'primary',
      size: <LoaderSize>'medium',
    },
    {
      type: <LoaderType>'infinite-spinner',
      themeColor: <LoaderThemeColor>'secondary',
      size: <LoaderSize>'medium',
    },
    {
      type: <LoaderType>'converging-spinner',
      themeColor: <LoaderThemeColor>'info',
      size: <LoaderSize>'medium',
    },
  ];
  //Variables related to kendo chart
  public hide = { visible: false };
  public labels = { color: '#6c757d', padding: 5, background: 'white' };
  public majorGridLines = { visible: false };
  public minorGridLines = { visible: false };
  public background = 'white';
  //Variables related to data in kendo chart
  public timeline!: { date: string; value: number }[];
  public series: any[] = []; //series is an array of timelines
  public daysInArray: number = 0; //var to be used to account data presented on graph

  private _calendarSearch!: calendarSearch;
  @Input() set calendarSearch(value: calendarSearch) {
    this._calendarSearch = value;
  }
  get calendarSearch() {
    return this._calendarSearch;
  }

  constructor(
    private api: DataServices,
    public accountService: AccountService
  ) {}

  ngOnInit(): void {
    console.log(this.consoleLogTag, 'sense minicard init...');

    console.log(this.consoleLogTag, this.databuckets);

    //subscribing to parent components to get updates on data requests per user
    this.csEventsSubscription = this.csEventChild.subscribe((data) => {
      console.log(this.consoleLogTag, 'Observable CalendarSearch');
      this._calendarSearch = <calendarSearch>data;
      console.log(this.consoleLogTag, this.calendarSearch);
      this.getDataSeries(this.id, this.sensorType);
    });

    this.getDataSeries(this.id, this.sensorType);
  }

  getParentType() {
    let parentType: string = 'NA';

    switch (this.type) {
      case 'building':
        parentType = 'tenant';
        break;
      case 'floor':
        parentType = 'building';
        break;
      case 'area':
        parentType = 'floor';
        break;
      case 'sensor':
        parentType = 'area';
        break;
    }
    return parentType;
  }

  getObjectType() {
    let objectType: any = "NA";

    if (this.type == 'NAType'){ //consideration for detail graph card
      this.type = this.accountService.navi.currentLocation;
    }

    switch (this.type) {
      case 'building':
        objectType = ObjectType.Building;
        break;
      case 'floor':
        objectType = ObjectType.Floor;
        break;
      case 'area':
        objectType = ObjectType.Area;
        break;
      case 'sensor':
        objectType = ObjectType.Sensor;
        break;
    }
    return objectType;
  }

  getSensorType() {
    let description: string = 'NA';
    switch (this.sensorType) {
      case '0':
        description = 'energy';
        break;
      case '1':
        description = 'occupancy';
        break;
      case '2':
        description = 'ligtlevel';
        break;
      case '3':
        description = 'sound';
        break;
      case '4':
        description = 'temperature';
        break;
      case '5':
        description = 'co2';
        break;
      case '6':
        description = 'humidity';
        break;
      case '7':
        description = 'gas';
        break;
    }
    return description;
  }

  getSensorTypeUnit() {
    let description: string = 'NA';
    switch (this.sensorType) {
      case '0':
        description = 'kwH';
        break;
      case '1':
        description = '% of occupancy';
        break;
      case '2':
        description = 'lux';
        break;
      case '3':
        description = 'db';
        break;
      case '4':
        description = 'centigrades';
        break;
      case '5':
        description = 'co2';
        break;
      case '6':
        description = '% of humidity';
        break;
      case '7':
        description = "m3";
        break;
    }
    return description;
  }

  getDataSeries(objectId: string, sensorType: string) {
    this.isDataReady = false; //while we load data
    console.log(
      this.consoleLogTag,
      'Getting data for series graph on objectId: ' +
        objectId +
        ' with SensorType: ' +
        sensorType
    );

    if (this.calendarSearch == undefined) {
      console.log(this.consoleLogTag, 'Calendar undefined generating new one.');
      this.calendarSearch = new calendarSearch();
      //this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate() - 1); //we add one day as its needed to recover 2 data rows
      console.log(this.consoleLogTag, this.calendarSearch);
    }

    let startDate = this.calendarSearch.minDate.toDateString();
    let endDate = this.calendarSearch.maxDate.toDateString();

    //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
    let tmpCalendar: calendarSearch = new calendarSearch();
    tmpCalendar.minDate = new Date(startDate);
    tmpCalendar.maxDate = new Date(endDate);
    tmpCalendar.timeInterval = this.calendarSearch.timeInterval; //possibly memory link need to review later
    tmpCalendar.weekDays = this.calendarSearch.weekDays; //possibly memory link need to review later

    let measure: Measurement = new Measurement(); //recommendation create object with constructor
    measure.CalendarSearch = tmpCalendar; //this.calendarSearch;
    measure.ObjectId = objectId; //this.building.id; //possibly memory link need to review later
    //measure.SensorType = SensorType.Energy;

    //This will let obtain data in 100% in case of any senser different than energy
    if(this.getSensorType() == "energy"){
      measure.MeasurementType = MeasurementType.Sum;
    } else {
      measure.MeasurementType = MeasurementType.Average;
    }
    

    //Define objectType based on the level selected on data structure
    measure.ObjectType = this.getObjectType(); 
    //ObjectType.Building; //redundant if we consider we provide objectI = building.id consider removing from logic in the future
    
    let numberOfDays = this._calendarSearch.getNumberOfDaysConsideredInCalculus();
    if(numberOfDays <= 2){
      measure.BucketTimeWindow = BucketTimeWindow.Hour;
      if (this.activateStructureFilter) {
        measure.BucketTimeWindow = BucketTimeWindow.Minute;
      }
    } else if (numberOfDays <= 7) {
      measure.BucketTimeWindow = BucketTimeWindow.Day;
      if (this.activateStructureFilter) {
        measure.BucketTimeWindow = BucketTimeWindow.Hour;
      } else {
      measure.BucketTimeWindow = BucketTimeWindow.Day; 
      }
    }
  
    console.log(this.consoleLogTag, measure);

    switch (sensorType) {
      case '0':
        measure.SensorType = SensorType.Energy;
        break;
      case '1':
        measure.SensorType = SensorType.Occupancy;
        break;
      case '2':
        measure.SensorType = SensorType.LightLevel;
        break;
      case '3':
        measure.SensorType = SensorType.Sound;
        break;
      case '4':
        measure.SensorType = SensorType.Temperature;
        break;
      case '5':
        measure.SensorType = SensorType.CO2;
        break;
      case '6':
        measure.SensorType = SensorType.Humidity;
        break;
    }

    this.timeline = [];
    this.series = [];

    this.api.getMeasurementArray(measure).subscribe(
      async (data: any) => {
        console.log(this.consoleLogTag, data);
        if (data !== null) {
          let datepipe: DatePipe = new DatePipe('en-EU');
          //let tmpV = { date: 'July 4', value: 99 };
          //this.timeline.push(tmpV);

          for (let i = 0; i < data.length; i++) {
            let stringDate: string = data[i].from;

            let value: number;
            if(this.getSensorType() == "energy"){
              value = data[i].sum;
            } else {
              value = data[i].avg;
            }
            

            let tmpDate: Date = new Date(stringDate);

            let formatedDate = datepipe.transform(tmpDate, 'MMMM d yyyy') ?? '';
            if (measure.BucketTimeWindow == BucketTimeWindow.Minute) {
              formatedDate =
                datepipe.transform(tmpDate, 'MMMM d yyyy, h:mm a') ?? '';
            } else if (measure.BucketTimeWindow == BucketTimeWindow.Hour) {
              formatedDate =
                datepipe.transform(tmpDate, 'MMMM d yyyy, h:mm a') ?? '';
            }

            let tmpTimeLine = { date: formatedDate, value: value };
            this.timeline.push(tmpTimeLine);
          }

          console.log(this.consoleLogTag, 'Data to show is:');
          console.log(this.consoleLogTag, this.timeline);

          this.daysInArray = this.timeline.length;
          this.series.push(this.timeline);

          this.isDataSummarized = false;
          this.isDataReady = true;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataSeries2(objectId: string, sensorType: string) {}

  valueChangeOnTree(value: any) {
    console.log(this.consoleLogTag, 'Changing value on tree...');
    console.log(this.consoleLogTag, value);
    this.id = value.id;
    this.description = value.description;
    this.getDataSeries(value.id, this.sensorType);
    this.idChange.emit(this.id);
    this.descriptionChange.emit(this.description);
  }
}
