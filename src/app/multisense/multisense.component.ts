import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Building } from '../models/building.model';
import { calendarSearch } from '../models/calendarSearch.model';
import { Measurement } from '../models/measurements/measurement';
import { DataServices } from '../services/data.service';
import { SensorType } from 'src/app/models/measurements/sensorType.model';
import { MeasurementType } from 'src/app/models/measurements/measurementType.model';
import { ObjectType } from 'src/app/models/measurements/objectType.model';
import { Subject, Subscription } from 'rxjs';
import { Energy } from '../models/energy.model';
import { AccountService } from '../services/account.service';
import { Databucket } from '../models/databucket.model';
import { faSleigh } from '@fortawesome/free-solid-svg-icons';
import { ValueTemplateDirective } from '@progress/kendo-angular-dropdowns';

@Component({
  selector: 'app-multisense',
  templateUrl: './multisense.component.html',
  styleUrls: ['./multisense.component.css'],
})
export class MultisenseComponent implements OnInit {
  public consoleLogTag: string = 'multisense';
  public isDataReady: boolean = false;
  public tenantId: string = 'tenantId';
  public sensors: string[] = ['0', '1', '4', '5', '6', '7']; //['0', '1', '2', '3', '4', '5', '6'];
  public filterBySensor: string = '0'; //by default we filter by Energy but we can use 'all' to put show sensors
  public filterByIndex: number = 0; //by default first value in array (on each building, floor or area)
  public filterByDetail: boolean = false; //we can show detail data on graph card
  public treeStructure: any[] = []; //for tree drop down
  public treeSelectedValue: any; //default value for tree dropdown

  private _building!: Building;
  @Input() set building(value: Building) {
    this._building = value;
  }
  get building() {
    return this._building;
  }

  private _calendarSearch!: calendarSearch;
  @Input() set calendarSearch(value: calendarSearch) {
    this._calendarSearch = value;
  }
  get calendarSearch() {
    return this._calendarSearch;
  }
  @Output() public _calendarSearchChange = new EventEmitter<string>();

  public daysInArray: number = 0; //var to be used to account data presented on graph
  public allowWeekDays: boolean = false;
  //pendiente implementar un get measurements que traiga la serie completa de datos no solo un valor

  //Variables related to kendo chart
  public hide = { visible: false };
  public labels = { color: '#6c757d', padding: 5, background: 'white' };
  public majorGridLines = { visible: false };
  public minorGridLines = { visible: false };
  public background = 'white';

  public series: any[] = []; //kendo data series

  csEventSubject: Subject<calendarSearch> = new Subject<calendarSearch>();

  //@Input() csEventChild!: Observable<calendarSearch>;
  //private csEventsSubscription!: Subscription;

  constructor(
    private api: DataServices,
    public accountService: AccountService
  ) {}

  ngOnInit(): void {
    console.log(this.consoleLogTag, 'Multisense init...');
    //this.building = new Building();

    let buildingId = 'e40ad79b-75ab-48b9-92a1-012e8c742b5c';
    //this.building.id = buildingId;

    this.loadStructure(buildingId);
    console.log(this.consoleLogTag, this.accountService.navi);
    //this.idChange.emit(this.id);
    //subscribing to parent components to get updates on data requests per user
  }

  loadStructure(objectId: string) {
    console.log(this.consoleLogTag, 'Loading structure...');

    if (this.calendarSearch == undefined) {
      console.log(this.consoleLogTag, 'Calendar undefined generating new one.');
      this.calendarSearch = new calendarSearch();
      //this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate() - 1); //we add one day as its needed to recover 2 data rows
      console.log(this.consoleLogTag, this.calendarSearch);
    }

    let startDate = this.calendarSearch.minDate.toDateString();
    let endDate = this.calendarSearch.maxDate.toDateString();

    //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
    let tmpCalendar: calendarSearch = new calendarSearch();
    tmpCalendar.minDate = new Date(startDate);
    tmpCalendar.maxDate = new Date(endDate);
    tmpCalendar.timeInterval = this.calendarSearch.timeInterval; //possibly memory link need to review later
    tmpCalendar.weekDays = this.calendarSearch.weekDays; //possibly memory link need to review later

    let measure: Measurement = new Measurement(); //recommendation create object with constructor
    measure.CalendarSearch = tmpCalendar; //this.calendarSearch;
    measure.ObjectId = objectId; //this.building.id; //possibly memory link need to review later
    measure.SensorType = SensorType.Energy;
    measure.MeasurementType = MeasurementType.Sum;
    measure.ObjectType = ObjectType.Building; //redundant if we consider we provide objectI = building.id consider removing from logic in the future

    this.api.getBuildingFullStructure(measure).subscribe(
      async (jsonObject: any) => {
        console.log(this.consoleLogTag, jsonObject);
        if (jsonObject !== null) {
          this.building = new Building(jsonObject);
          console.log(this.consoleLogTag, this.building);

          this.accountService.navi.buildingIndex = 0;
          //this.accountService.navi.currentLocation='building';
          this.treeStructure = this.getBuildingStructure();
          this.treeSelectedValue = this.getCurrenLocationStructure();
          this.isDataReady = true;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataSeries(objectId: string) {
    console.log(
      this.consoleLogTag,
      'Objetd data to be loaded from ID: ' + objectId
    );
  }

  getDataSeries2(objectId: string) {
    if (this.calendarSearch == undefined) {
      console.log(this.consoleLogTag, 'Calendar undefined generating new one.');
      this.calendarSearch = new calendarSearch();
      //this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate() - 1); //we add one day as its needed to recover 2 data rows
      console.log(this.consoleLogTag, this.calendarSearch);
    }

    let startDate = this.calendarSearch.minDate.toDateString();
    let endDate = this.calendarSearch.maxDate.toDateString();

    //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
    let tmpCalendar: calendarSearch = new calendarSearch();
    tmpCalendar.minDate = new Date(startDate);
    tmpCalendar.maxDate = new Date(endDate);
    tmpCalendar.timeInterval = this.calendarSearch.timeInterval; //possibly memory link need to review later
    tmpCalendar.weekDays = this.calendarSearch.weekDays; //possibly memory link need to review later

    let measure: Measurement = new Measurement(); //recommendation create object with constructor
    measure.CalendarSearch = tmpCalendar; //this.calendarSearch;
    measure.ObjectId = objectId; //this.building.id; //possibly memory link need to review later
    measure.SensorType = SensorType.Energy;
    measure.MeasurementType = MeasurementType.Sum;
    measure.ObjectType = ObjectType.Building; //redundant if we consider we provide objectI = building.id consider removing from logic in the future

    this.api.getMeasurementArray(measure).subscribe(
      async (jsonObject: any) => {
        console.log(this.consoleLogTag, jsonObject);
        if (jsonObject !== null) {
          //this.building = new Building(jsonObject);
          this.setDatabuckets(jsonObject);
          console.log(this.consoleLogTag, this.building);

          //this.accountService.navi.buildingIndex=0;
          //this.accountService.navi.currentLocation='building';
          //this.isDataReady = true;
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  setDatabuckets(jsonObject: any) {
    switch (this.accountService.navi.currentLocation) {
      case 'building':
        this.building.databuckets = [];
        for (let i = 0; i < jsonObject.length; i++) {
          let databucket = new Databucket(jsonObject);
          this.building.databuckets.push(databucket);
        }
        break;
      case 'floor':
        this.building.floors[this.accountService.navi.floorIndex].databuckets =
          [];
        for (let i = 0; i < jsonObject.length; i++) {
          let databucket = new Databucket(jsonObject);
          this.building.floors[
            this.accountService.navi.floorIndex
          ].databuckets.push(databucket);
        }
        break;
      case 'area':
        this.building.floors[this.accountService.navi.floorIndex].areas[
          this.accountService.navi.areaIndex
        ].databuckets = [];
        for (let i = 0; i < jsonObject.length; i++) {
          let databucket = new Databucket(jsonObject);
          this.building.floors[this.accountService.navi.floorIndex].areas[
            this.accountService.navi.areaIndex
          ].databuckets.push(databucket);
        }
        break;
    }
  }

  async SearchEvent(search: calendarSearch) {
    let newSearch = search;
    this.calendarSearch = search;
    //await this.energyService.getBuildingEnergySavings(this.loadedFloors, newSearch);
    //await this.energyService.getBuildingEnergySavings(this.buildings[0].floors, search);
    console.log(this.consoleLogTag, 'Search event triggered');
    console.log(this.consoleLogTag, search);
    //this.searchClick.emit(search);
    this.csEventSubject.next(search);
    //this.energiesEventSubject.next(this.parentEnergies);
    //this.buildingEventSubject.next(this.buildings[0]); //esto no deberia ir aqui sino en el BuildingSelectEvent.. el cual no existe aun
    //esto debe ligarse a un observable que pueda pasar despues al hijo (output) los valores para mostrarlos
  }

  setCurrentLocation(location: string, objectIndex: number) {
    this.filterByIndex = objectIndex; //we indicate to navigation bar that we will be positioned on index selected by button
    this.accountService.navi.currentLocation = location;
    this.filterByDetail = false;

    switch (location) {
      case 'building':
        this.accountService.navi.buildingIndex = objectIndex;
        this.filterByDetail = false;
        break;
      case 'floor':
        this.accountService.navi.floorIndex = objectIndex;
        this.filterByDetail = false;
        break;
      case 'area':
        this.accountService.navi.areaIndex = objectIndex;
        //this.accountService.navi.currentLocation = 'floor'; //we force to stay on that level
        this.filterByDetail = true;
        break;
      case 'sensor':
        this.accountService.navi.channelIndex = objectIndex;
        this.accountService.navi.currentLocation = 'floor'; //we force to stay on that level
        break;
    }
  }

  getSensorCardsBy(sensor: string) {
    this.filterBySensor = 'all'; //default all
    switch (sensor) {
      case 'energy':
        this.filterBySensor = '0';
        break;
      case 'occupancy':
        this.filterBySensor = '1';
        break;
      case 'lightlevel':
        this.filterBySensor = '2';
        break;
      case 'sound':
        this.filterBySensor = '3';
        break;
      case 'temperature':
        this.filterBySensor = '4';
        break;
      case 'co2':
        this.filterBySensor = '5';
        break;
      case 'humidity':
        this.filterBySensor = '6';
        break;
      case 'gas':
        this.filterBySensor = '7';
        break;
    }
  }

  getSensorType() {
    let sensorType: string = 'NA';
    switch (this.filterBySensor) {
      case '0':
        sensorType = 'energy';
        break;
      case '1':
        sensorType = 'occupancy';
        break;
      case '2':
        sensorType = 'lightlevel';
        break;
      case '3':
        sensorType = 'sound';
        break;
      case '4':
        sensorType = 'temperature';
        break;
      case '5':
        sensorType = 'co2';
        break;
      case '6':
        sensorType = 'humidity';
        break;
      case '7':
        sensorType = 'gas';
        break;
    }
    return sensorType;
  }

  filterBy(level: string, indexSelected: number) {
    this.filterByIndex = indexSelected;

    switch (level) {
      case 'building':
        break;
      case 'floor':
        break;
      case 'area':
        this.accountService.navi.floorIndex = indexSelected;
        break;
      case 'sensor':
        break;
    }
    console.log(this.consoleLogTag, 'Index selected: ' + indexSelected);
    console.log(this.consoleLogTag, this.accountService);
  }

  getCurrentLocationId() {
    let currentLocationId: string = 'NA';

    switch (this.accountService.navi.currentLocation) {
      case 'building':
        currentLocationId = this.building.id;
        break;
      case 'floor':
        currentLocationId =
          this.building.floors[this.accountService.navi.floorIndex].id;
        break;
      case 'area':
        currentLocationId =
          this.building.floors[this.accountService.navi.floorIndex].areas[
            this.accountService.navi.areaIndex
          ].id;
        break;
    }
    return currentLocationId;
  }

  setCurrentLocationId(id: string) {
    switch (this.accountService.navi.currentLocation) {
      case 'building':
        this.building.id = id;
        break;
      case 'floor':
        this.building.floors[this.accountService.navi.floorIndex].id = id;
        break;
      case 'area':
        this.building.floors[this.accountService.navi.floorIndex].areas[
          this.accountService.navi.areaIndex
        ].id = id;
        break;
    }
  }

  findCurrentLocation(id: string) {
    let currentLocation: string = 'NA';

    if (this.building.id == id) {
      currentLocation = 'building';
      this.accountService.navi.currentLocation = currentLocation;
      this.accountService.navi.buildingIndex = 0;
    } else {
      let i = 0;
      this.building.floors.forEach((floor) => {
        if (floor.id == id) {
          currentLocation = 'floor';
          this.accountService.navi.currentLocation = currentLocation;
          this.accountService.navi.floorIndex = i;
        } else {
          let j = 0;
          floor.areas.forEach((area) => {
            if (area.id == id) {
              currentLocation = 'area';
              this.accountService.navi.currentLocation = currentLocation;
              this.accountService.navi.floorIndex = i;
              this.accountService.navi.areaIndex = j;
            }
            j++;
          });
        }
        i++;
      });
    }
  }

  getCurrentLocationDescription() {
    let currentLocation: string = 'NA';

    switch (this.accountService.navi.currentLocation) {
      case 'building':
        currentLocation = this.building.description;
        break;
      case 'floor':
        currentLocation =
          this.building.floors[this.accountService.navi.floorIndex].description;
        break;
      case 'area':
        currentLocation =
          this.building.floors[this.accountService.navi.floorIndex].areas[
            this.accountService.navi.areaIndex
          ].description;
        break;
    }
    return currentLocation;
  }

  getCurrentLocationDatabucket() {
    let currentLocation: Databucket[] = [];

    switch (this.accountService.navi.currentLocation) {
      case 'building':
        currentLocation = this.building.databuckets;
        break;
      case 'floor':
        currentLocation =
          this.building.floors[this.accountService.navi.floorIndex].databuckets;
        break;
      case 'area':
        currentLocation =
          this.building.floors[this.accountService.navi.floorIndex].areas[
            this.accountService.navi.areaIndex
          ].databuckets;
        break;
    }
    return currentLocation;
  }

  getBuildingStructure(): any[] {
    let data: any[] = [];
    let floorStructure: any[] = [];

    let buildingStructure = {
      text: 'Building: ' + this.building.description,
      id: this.building.id,
      items: floorStructure,
    };

    this.building.floors.forEach((floor) => {
      let areaStructure: any[] = [];

      let tmpFloorStructure = {
        text: 'Floor: ' + floor.description,
        id: floor.id,
        items: areaStructure,
      };

      floor.areas.forEach((area) => {
        let tmpAreaStructure = {
          text: 'Area: ' + area.description,
          id: area.id,
        };

        tmpFloorStructure.items.push(tmpAreaStructure);
      });

      buildingStructure.items.push(tmpFloorStructure);
    });

    data.push(buildingStructure);
    return data;
  }

  getCurrenLocationStructure(): any {
    let value: { text: string; id: string } = { text: 'NA', id: 'NA' };

    console.log(this.consoleLogTag, this.accountService.navi.currentLocation);
    switch (this.accountService.navi.currentLocation) {
      case 'building':
        value.id = this.building.id;
        value.text = 'Building: ' + this.building.description;
        break;
      case 'floor':
        value.id = this.building.floors[this.accountService.navi.floorIndex].id;
        value.text =
          'Floor: ' +
          this.building.floors[this.accountService.navi.floorIndex].description;
        break;
      case 'area':
        value.id =
          this.building.floors[this.accountService.navi.floorIndex].areas[
            this.accountService.navi.areaIndex
          ].id;
        value.text =
          'Area: ' +
          this.building.floors[this.accountService.navi.floorIndex].areas[
            this.accountService.navi.areaIndex
          ].description;
        break;
    }
    console.log(this.consoleLogTag, value);
    console.log(this.consoleLogTag, this.getBuildingStructure());
    return value;
  }
}
