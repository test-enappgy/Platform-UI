import { Component, OnInit } from '@angular/core';
import { PlotBand } from "@progress/kendo-angular-charts";

@Component({
  selector: 'app-environment',
  templateUrl: './environment.component.html',
  styleUrls: ['./environment.component.css']
})
export class EnvironmentComponent implements OnInit {

  public environmentVariables: {variable: string, average: number, target: number, unit: string}[] = [
    {variable: 'Temperature', average: 22.3, target: 21.0, unit: '°C'},
    {variable: 'CO2', average: 800, target: 750, unit: 'ppm'},
    {variable: 'Humidity', average: 45, target: 40, unit: '%'}
  ]

  public hidden: any = { visible: false };
  public tempPlotBands: PlotBand[] = [
    {
      from: 30,
      to: 45,
      color: "#e62325",
      opacity: 1,
    },
    {
      from: 15,
      to: 30,
      color: "#ffc000",
      opacity: 1,
    },
    {
      from: 0,
      to: 15,
      color: "#37b400",
      opacity: 1,
    },
    {
      from: -10,
      to: 0,
      color: "#5392ff",
      opacity: 1,
    },
  ];
  public humPlotBands: PlotBand[] = [
    {
      from: 0,
      to: 33,
      color: "#ccc",
      opacity: 0.6,
    },
    {
      from: 33,
      to: 66,
      color: "#ccc",
      opacity: 0.3,
    },
  ];
  public mmhgPlotBands: PlotBand[] = [
    {
      from: 715,
      to: 752,
      color: "#ccc",
      opacity: 0.6,
    },
    {
      from: 752,
      to: 900,
      color: "#ccc",
      opacity: 0.3,
    },
  ];
  public temp: any[] = [[25, 22]];
  public hum: any[] = [[45, 60]];
  public mmhg: any[] = [[800, 850]];
  constructor() { }

  ngOnInit(): void {
  }

}
