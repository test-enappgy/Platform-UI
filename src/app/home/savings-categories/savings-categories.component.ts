import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-savings-categories',
  templateUrl: './savings-categories.component.html',
  styleUrls: ['./savings-categories.component.css']
})
export class SavingsCategoriesComponent implements OnInit {

  public autofit = true;
  /*
  [{
    category: 'Lighting',
    value: 380
  },
  {
    category: 'Heating, Ventilation, and Cooling',
    value: 120
  },
  {
    category: 'Occupancy',
    value: 80
  }]
  */
  public data: {category: string, value: number}[] = [{
    category: 'lighting',
    value: 380
  },
  {
    category: 'heating, ventilation, and cooling',
    value: 0
  },
  {
    category: 'occupancy',
    value: 0
  }]

  public testData: any[] = [
    {
      kind: "Solar",
      share: 0.052,
    },
    {
      kind: "Wind",
      share: 0.225,
    },
    {
      kind: "Other",
      share: 0.192,
    },
    {
      kind: "Hydroelectric",
      share: 0.175,
    },
    {
      kind: "Nuclear",
      share: 0.238,
    },
    {
      kind: "Coal",
      share: 0.118,
    },
  ];

  public sumOfValues = this.data.reduce((accum, item) => accum + item.value, 0)

  public labelContent(e: any): string {
    return e.category;
  }
  constructor() { }
  ngOnInit(): void {
  }

}
