import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { Building } from '../models/building.model';
import { calendarSearch } from '../models/calendarSearch.model';
import { Energy } from '../models/energy.model';
import { Floor } from '../models/floor.model';
import { AccountService } from '../services/account.service';
import { DataServices } from '../services/data.service';
import { EnergyService } from '../services/energy.service';
import { environment as env } from 'src/environments/environment';
import { Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public allowWeekDays:boolean = false;
  buildings: Array<Building> = [];
  loadedFloors!: Floor[];
  calendarSearch: calendarSearch = new calendarSearch();
  public background = 'white';
  private profileJson:string = '';
  energyConsumption!: Energy;
  loadMultiLayer: boolean = false;
  totalEnergy: number = 0;
  totalEnergyData: {date: string, value: number}[] = new Array();
  loadCalendar: boolean = true;

  isStructureLoaded: boolean = false;
  isDataEnergyLoaded: boolean = false;
  parentBuilding !: Building;
  //@Output() searchClick: EventEmitter<calendarSearch> = new EventEmitter();
  csEventSubject: Subject<calendarSearch> = new Subject<calendarSearch>();
  buildingEventSubject: Subject<Building> = new Subject<Building>();
  energiesEventSubject: Subject<Energy[]> = new Subject<Energy[]>();
  /*emitEventToChild(){
    this.csEventSubject.next();
    this.buildingEventSubject.next();
  }*/
  public parentEnergies: Energy[] = []; //this property is for energy savings card [previous, current]
  public isEnergyEventReady: boolean = false;

  consoleLogTag = "home";

  constructor(private api: DataServices, private accountService: AccountService, private auth: AuthService, private energyService: EnergyService) { }

  ngOnInit(): void {
    let tenant = env.enappgyData.tenantId;
    this.api.setTenantId(tenant);
    this.api.setTimeRange(1);
    this.auth.user$.subscribe((profile) => {
        this.profileJson = JSON.stringify(profile, null, 2);
        let profileId:string = '';
        if (profile?.sub != null){
          profileId = profile?.sub;
          this.api.setTenantId(profile.userMetadata.tenantId);
          this.api.setDateFrom(new Date());
          let date2 = new Date();
          date2.setDate(date2.getDate()+this.api.getTimeRange());
          this.api.setDateTill(date2);
          this.accountService.loadFromSessionStorage();
        }
      }
    );
    this.loadBuilding(); //AQUI DEBEMOS METER UNA LLAMADA PARA OBTENER EL BUILDING DEL TENANTID
    //DEBEMOS DEFINIR DE DONDE VENDRA RELACION CUENTA CON BUILDING
  }

  async loadBuilding() {
    this.api.getBuildings().subscribe(
      (buildings: any) => {
        console.log(this.consoleLogTag, buildings);
        let building = new Building(buildings);
        console.log(this.consoleLogTag, building);
        this.buildings.push(building);
        this.parentBuilding = this.buildings[0];
        this.isStructureLoaded = true;
        /*this.buildings.forEach(floor =>{
          this.loadFloors(floor.id); //no es necesario de momento
        });*/
        /*if(buildings.length > 0) {
          this.buildings = buildings;
          console.log("structure", buildings);
          console.log("structure",this.buildings);
          this.buildings.forEach(floor => {
            this.loadFloors(floor.id);
          });
        }*/
        this.loadCalendar = true;
        console.log(this.consoleLogTag, this.calendarSearch);
        this.SearchEvent(this.calendarSearch);

        /*Si disparamos el searchevent esto genera una funcion de promesa
        pero necesitamos que sea un observable de tal forma que al regresar datos podamos
        pasarselos al hijo
        o...
        al menos en el primer evento usar un ngIf para que no cargue nada hasta no tener todo
        */

      },
      err => {
        console.log(err);
      }
    );
  }

  async loadFloors(floorId: string) {
    this.api.getFloors(floorId).subscribe(
    (floors: any) => {
      if(floors !== null) {
        this.loadedFloors = floors; //esto solo almacena la ultima iteracion se esta sobre escribiendo
        floors.forEach((f:Floor)=> {
          //aqui hay que guardar estos pisos en estrcutura o se pierde la data
        });
        this.loadCalendar = true;
        //esta linea no existia y es parte del trigger
        //this.SearchEvent(this.calendarSearch);
      }
    },
    err => {
      console.log(err);
    });
  }

  async SearchEvent(search: calendarSearch) {
      let newSearch = search;
      this.calendarSearch = search;
      //await this.energyService.getBuildingEnergySavings(this.loadedFloors, newSearch);
      //await this.energyService.getBuildingEnergySavings(this.buildings[0].floors, search);
      console.log(this.consoleLogTag, "Search event triggered");
      console.log(this.consoleLogTag, search);
      //this.searchClick.emit(search);
      this.csEventSubject.next(search);
      this.energiesEventSubject.next(this.parentEnergies);
      //this.buildingEventSubject.next(this.buildings[0]); //esto no deberia ir aqui sino en el BuildingSelectEvent.. el cual no existe aun
      //esto debe ligarse a un observable que pueda pasar despues al hijo (output) los valores para mostrarlos
  }

  /*Tenemos 2 energy service uno en padre y otro en hijo, no crteo que tengan ligada la misma informacion, seria mas bien manejar todo en el padre
y que le lleguen al hijo solo los datos requeridos. Por tanto el getEnergyPreviousYear debe amarrarse al padre no al hijo
o...
le pasamos al hijo el building y que el arme su propio dato
si eso conviene que el hijo reciba continuamente el building y el calendarsearch */

  async energyDataLoadEvent(energies: Energy[]) {
    console.log("home", "Energy dataload event triggered");
    this.parentEnergies = energies;
    this.isEnergyEventReady = true;
    console.log("home", energies);
    this.energiesEventSubject.next(energies); //emitimos una actualizacion al subject
  }
}
