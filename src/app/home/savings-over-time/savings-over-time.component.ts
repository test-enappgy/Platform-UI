import { Component, Input, OnInit } from '@angular/core';
import { Observable, Observer, Subscription } from 'rxjs';
import { Building } from 'src/app/models/building.model';
import { calendarSearch } from 'src/app/models/calendarSearch.model';
import { Energy } from 'src/app/models/energy.model';
import { EnergyService } from 'src/app/services/energy.service';
import { SensorType } from 'src/app/models/measurements/sensorType.model';
import { MeasurementType } from 'src/app/models/measurements/measurementType.model';
import { ObjectType } from 'src/app/models/measurements/objectType.model';
import { DatePipe } from '@angular/common';
import { Floor } from 'src/app/models/floor.model';
import { Measurement } from 'src/app/models/measurements/measurement';
import { DataServices } from 'src/app/services/data.service';

@Component({
  selector: 'app-savings-over-time',
  templateUrl: './savings-over-time.component.html',
  styleUrls: ['./savings-over-time.component.css'],
})
export class SavingsOverTimeComponent implements OnInit {
  private consoleLogTag: String = 'savingsOverTime';
  //public baselineConsumptionConstant = 755.36;
  public daysInArray: number = 0; //var to be used to account data presented on graph

  private _building!: Building;
  @Input() set building(value: Building) {
    this._building = value;
  }
  get building() {
    return this._building;
  }

  private _calendarSearch!: calendarSearch;
  @Input() set calendarSearch(value: calendarSearch) {
    this._calendarSearch = value;
  }
  get calendarSearch() {
    return this._calendarSearch;
  }

  private _energies!: Energy[];
  @Input() set energies(value: Energy[]) {
    this._energies = value;
  }
  get energies() {
    return this._energies;
  }

  @Input() csEventChild!: Observable<calendarSearch>;
  private csEventsSubscription!: Subscription;

  @Input() buildingEventChild!: Observable<Building>;
  private buildingEventSubscription!: Subscription;

  //deprecated
  @Input() energiesEventChild!: Observable<Energy[]>; //to receive data from parent from current vs baseline card
  private energiesEventSubscription!: Subscription;

  public hide = { visible: false };
  public labels = { color: '#6c757d', padding: 5, background: 'white' };
  public majorGridLines = { visible: false };
  public minorGridLines = { visible: false };
  public background = 'white';

  public series: any[] = []; //kendo series

  energySavings: Energy = new Energy();
  getEnergySavings(): any {
    //let res = [{date: 'June 16', value: 1054.5243093883034}, {date: 'June 17', value: 1004.5243093883034}];
    //return res;
    return this.energySavings.timeLine;
  }
  isEnergySavings: boolean = false;

  energyData: Energy[] = []; //data for kendo
  energyCategories: any[] = []; //for kendo bar graph

  energyToolTip: String[] = [];
  getEnergyToolTip(): String[] {
    let toolTips: String[] = [];
    this.energySavings.consolidatedTimeLine().forEach((result) => {
      toolTips.push(result.value.toString());
    });
    return toolTips;
  }

  private _baselineConsumption: Energy = new Energy();
  public addToBaselineConsumption(value: number) {
    this._baselineConsumption.total += value;
    //this.energyDataLoad.emit(this.getEnergyDataArray()); //invalidamos esta linea solo requerimos la de current para evitar duplicidad en envio de datos y optimizar stream
  }
  public getBaselineConsumption(): number {
    return this._baselineConsumption.total;
  }

  constructor(private energyService: EnergyService, private api: DataServices) {
    this.energyService.getEnergySavings$().subscribe((data) => {
      //this.energySavings = data;
    });
  }

  ngOnInit(): void {
    console.log(this.consoleLogTag, 'Init of Savings over time card');
    //this.addToBaselineConsumption(this.baselineConsumptionConstant);
    this.addToBaselineConsumption(this.energySavings.baseline);
    //this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate() -  1);
    this.getEnergySavingsData();

    //subscribing to parent components to get updates on data requests per user
    this.csEventsSubscription = this.csEventChild.subscribe((data) => {
      console.log(this.consoleLogTag, 'Observable CalendarSearch');
      this._calendarSearch = <calendarSearch>data;
      console.log(this.consoleLogTag, this.calendarSearch);
      this.getEnergySavingsData();
    });

    this.buildingEventSubscription = this.buildingEventChild.subscribe(
      (data) => {
        console.log(this.consoleLogTag, 'Observable Building');
        this._building = <Building>data;
        this.getEnergySavingsData();
      }
    );

    /*
     //YA NO SE REQUIERE VAMOS A RE CONSTRUIR DE FORMA INDEPENDIENTE SIMPLIFICANDO LLAMADAS
     //si dejamos la logica de que esta tarjeta observe a su hermana no necesitamos los observables de arriba
     //ya que la tarjeta hermana lo hace
     this.energiesEventSubscription = this.energiesEventChild.subscribe(data => {
       console.log(this.consoleLogTag, "Observable Energies dataload");
       this._energies = <Energy[]>data;
       //this.calculateEnergySavings(); //revisar este metodo ya que solo debe calcular y mostrar usando propiedades internas
       this.energySavings = this.energyService.calculateEnergySavings(this._energies[0], this._energies[1]); //ESTE LLAMA A ENERGY SERVICE NO AL LOCAL
       //this.energySavings = this.energySavings.consolidateTimeLine();
       this.energyCategories = this.energySavings.consolidatedTimeLine();
       console.log("energy", this.energyCategories);
     },
     err => {
       console.log(this.consoleLogTag, err);
     }
     );
     */
  }

  async getEnergySavingsData() {
    console.log(this.consoleLogTag, 'Calculating Energy Savings...');
    console.log(this.consoleLogTag, this.calendarSearch);
    this.energySavings = new Energy();
    //this.energySavings.timeLine = [];
    this.energyCategories = [];

    //this if loads on first time as object has not loaded completely and event emmiter doesnt arrives
    if (this.calendarSearch == undefined) {
      console.log(this.consoleLogTag, "Calendar undefined generating new one.")
      this.calendarSearch = new calendarSearch();
      //this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate() - 1); //we add one day as its needed to recover 2 data rows
      console.log(this.consoleLogTag, this.calendarSearch);
    }

    let startDate = this.calendarSearch.minDate.toDateString();
    let endDate = this.calendarSearch.maxDate.toDateString(); //new Date(this.calendarSearch.maxDate.getDate() - tmpDays).toDateString();

    let tmpDate = new Date(this.calendarSearch.minDate.toDateString());

    let tmpDays = 0;
    //this logic below is for current day, as we need to provide 2 days to the loop
    if ( this.calendarSearch.minDate.getDate() == this.calendarSearch.maxDate.getDate()) {
      console.log(this.consoleLogTag, 'Dates match!');
      //tmpCalendar.minDate.setDate(tmpCalendar.minDate.getDate() -  1);
      //tmpDays = 1;
    }

    tmpDate.setDate(tmpDate.getDate() - tmpDays);
    startDate = tmpDate.toDateString();
    console.log(this.consoleLogTag, 'Processing data from: ' + startDate + ' to: ' + endDate);

    let datepipe: DatePipe = new DatePipe('en-EU');

    let dateRange = this.energyService.getDatesBetween(startDate, endDate);
    console.log(this.consoleLogTag, dateRange);

    //this.energySavings.timeLine = [{date: 'June 15', value: 1054.5243093883034}];

    //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
    let tmpCalendar: calendarSearch = new calendarSearch();
    tmpCalendar.minDate = new Date(startDate);
    tmpCalendar.maxDate = new Date(endDate);
    tmpCalendar.timeInterval = this.calendarSearch.timeInterval; //possibly memory link need to review later
    tmpCalendar.weekDays = this.calendarSearch.weekDays; //possibly memory link need to review later

    let measure: Measurement = new Measurement(); //recommendation create object with constructor
    measure.CalendarSearch = tmpCalendar; //this.calendarSearch;
    measure.ObjectId = this.building.id; //possibly memory link need to review later
    measure.SensorType = SensorType.Energy;
    measure.MeasurementType = MeasurementType.Sum;
    measure.ObjectType = ObjectType.Building; //redundant if we consider we provide objectI = building.id consider removing from logic in the future

    console.log(this.consoleLogTag, measure);

    this.series = [];

    this.api.getMeasurementArray(measure).subscribe(
      (data: any) => {
        console.log(this.consoleLogTag, 'Getting measures array...');
        if (data != undefined) {
          console.log(this.consoleLogTag, data);

          for (let i = 0; i < data.length; i++) {
            //console.log(this.consoleLogTag,"Inside For loop iteration " + i);
            let stringDate: string = data[i].from;
            let tmpValue: number = ((this.getBaselineConsumption() - data[i].sum)); //savings in KwH we multiply on -1 to ensure savings are positive

            let tmpDate: Date = new Date(stringDate);
            let formatedDate = datepipe.transform(tmpDate, 'MMMM d') ?? '';

            let tmpTimeLine = { date: formatedDate, value: tmpValue };
            this.energySavings.timeLine.push(tmpTimeLine);

            //console.log(this.consoleLogTag, "Adding data to timeline current data is:");
            //console.log(this.consoleLogTag, this.energySavings.timeLine);
            //this.energyCategories = this.energySavings.consolidatedTimeLine();
          }

          this.daysInArray = this.energySavings.timeLine.length;
          //this.minorGridLines.visible = true;


          console.log(this.consoleLogTag, "Data to show is:");
          console.log(this.consoleLogTag, this.energySavings.timeLine);

          this.series.push(this.energySavings);
        }

        if (this.energySavings.timeLine.length == dateRange.length) {
          this.isEnergySavings = true;
          console.log(this.consoleLogTag,"Energy is TRUE")
          //this.energySavings.timeLine.splice(0, 1); //removemos 1 valor
        } else {
          this.isEnergySavings = false;
          console.log(this.consoleLogTag, "ENERGY is FALSE");
        }

        console.log(this.consoleLogTag, this.isEnergySavings);
      },
      (err) => {
        console.log(this.consoleLogTag, err);
      }
    );
  }


  getDataOld(startDate: any, endDate: any) {
    let datepipe: DatePipe = new DatePipe('en-EU');
    let dateRange = this.energyService.getDatesBetween(startDate, endDate);

    dateRange.forEach((date: Date) => {
      console.log(this.consoleLogTag, 'Iterating on daterange: ' + date);

      let measurement: Measurement = new Measurement();

      //this.calendarSearch.setMinDate(date);
      //this.calendarSearch.setMaxDate(date);

      //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
      let tmpCalendar: calendarSearch = new calendarSearch();
      tmpCalendar.minDate = new Date(date);
      tmpCalendar.maxDate = new Date(date);

      /*if(tmpCalendar.minDate.getDate() == tmpCalendar.maxDate.getDate()){
        console.log(this.consoleLogTag, "Dates match!");
        tmpCalendar.minDate.setDate(tmpCalendar.minDate.getDate() -  1);
      }*/
      tmpCalendar.timeInterval = this.calendarSearch.timeInterval;
      tmpCalendar.weekDays = this.calendarSearch.weekDays;

      measurement.CalendarSearch = tmpCalendar; //this.calendarSearch;
      measurement.ObjectId = this.building.id;
      measurement.SensorType = SensorType.Energy;
      measurement.MeasurementType = MeasurementType.Sum;
      measurement.ObjectType = ObjectType.Building;

      console.log(this.consoleLogTag, measurement);

      this.api.getMeasurement(measurement).subscribe(
        //esto no devuelve datos por la API hay que revisar con Jeffrey
        (data: any) => {
          console.log(this.consoleLogTag, 'Getting measures');
          if (data != undefined) {
            console.log(this.consoleLogTag, data);

            //this.energySavings = this.energyService.calculateEnergySavings(this._energies[0], this._energies[1]); //ESTE LLAMA A ENERGY SERVICE NO AL LOCAL
            let value: number = 0;
            if (data.sum != 0) {
              value = data.sum - this.getBaselineConsumption();
            }

            let formatedDate = datepipe.transform(date, 'MMMM d') ?? '';

            let newMeasure = { date: formatedDate, value: value / 1000 };
            this.energySavings.timeLine.push(newMeasure);
            console.log(this.consoleLogTag, newMeasure);
            console.log(this.consoleLogTag, this.energySavings.timeLine);

            //this.energyData.push();
            this.energyCategories = this.energySavings.consolidatedTimeLine();

            console.log(this.consoleLogTag, this.energyCategories);
            if (this.energySavings.timeLine.length == dateRange.length) {
              this.isEnergySavings = true;
              this.energySavings.timeLine.splice(0, 1); //removemos 1 valor
            } else {
              this.isEnergySavings = false;
            }
          }
        },
        (err) => {
          console.log(this.consoleLogTag, err);
        }
      );
    });
  }
}
