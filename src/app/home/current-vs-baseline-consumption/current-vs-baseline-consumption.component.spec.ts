import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CurrentVsBaselineConsumptionComponent } from './current-vs-baseline-consumption.component';

describe('CurrentVsBaselineConsumptionComponent', () => {
  let component: CurrentVsBaselineConsumptionComponent;
  let fixture: ComponentFixture<CurrentVsBaselineConsumptionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CurrentVsBaselineConsumptionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CurrentVsBaselineConsumptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
