import { Component, Input, OnInit, Output, SimpleChanges, EventEmitter } from '@angular/core';
import { EnergyService } from 'src/app/services/energy.service';
import { Observable, Observer, Subscription } from 'rxjs';
import { Building } from 'src/app/models/building.model';
import { calendarSearch } from 'src/app/models/calendarSearch.model';
//import { clearInterval } from 'timers';
import { Energy } from "src/app/models/energy.model";
import { Floor } from 'src/app/models/floor.model';
import { Measurement } from 'src/app/models/measurements/measurement';
import { SensorType } from 'src/app/models/measurements/sensorType.model';
import { MeasurementType } from 'src/app/models/measurements/measurementType.model';
import { ObjectType } from 'src/app/models/measurements/objectType.model';
import { DataServices } from 'src/app/services/data.service';
import { DatePipe } from "@angular/common";

@Component({
  selector: 'app-current-vs-baseline-consumption',
  templateUrl: './current-vs-baseline-consumption.component.html',
  styleUrls: ['./current-vs-baseline-consumption.component.css']
})
export class CurrentVsBaselineConsumptionComponent implements OnInit {

  public consoleLogTag = "consumption";
  //public baselineConsumptionConstant = 1755.36;
  public energy: Energy = new Energy(); //metimos la constante al objeto para encapuslar el valor y tener mas control conforme se propague en el proyecto

  private _building !: Building;
  @Input() set building(value:Building) { this._building = value }
  get building() { return this._building }
  private _calendarSearch !: calendarSearch
  @Input() set calendarSearch(value:calendarSearch) {this._calendarSearch = value }
  get calendarSearch() { return this._calendarSearch }
  
  @Input() csEventChild !: Observable<calendarSearch>;
  private csEventsSubscription !: Subscription;

  @Input() buildingEventChild !: Observable<Building>;
  private buildingEventSubscription !: Subscription;

  @Input() cConsumptionObv !: Observable<number>;
  private cConsumptionSubscription !: Subscription;

  @Input() pConsumptionObv !: Observable<number>;
  private pConsumptionSubscription !: Subscription;

  //public localBuilding !: Building;
  //public localCalendarSearch !: calendarSearch;
  
  private _baselineConsumption : Energy = new Energy(); //esto podria ser un objeto numero no se usa nada mas
  public addToBaselineConsumption(value:number) { 
    this._baselineConsumption.total += value; 
    //this.energyDataLoad.emit(this.getEnergyDataArray()); //invalidamos esta linea solo requerimos la de current para evitar duplicidad en envio de datos y optimizar stream
  }
  public getBaselineConsumption() : number { return this._baselineConsumption.total }
  public resetBaseLineConsumption() { this._baselineConsumption.total = 0; }
  
  private _currentConsumption : Energy = new Energy(); //esto podria ser un objeto numero no se usa nada mas
  public addToCurrentConsumption(value:number) { 
    this._currentConsumption.total += value; 

    console.log(this.consoleLogTag, "Emitiendo energyLoad")
    //this.energyDataLoad.emit(this.getEnergyDataArray()); //enviamos a padre para que se use en saving card
  }
  public getCurrentConsumption() : number { return this._currentConsumption.total }
  public resetCurrentConsumption(){ this._currentConsumption.total = 0; }

  //@Output() energyDataLoad: EventEmitter<Energy[]> = new EventEmitter(); //for saving card calculation, to send Energy array (previous, actual)

  /*
  public getEnergyDataArray() : Energy[] {
    let energies : Energy[] = [];
    energies.push(this._baselineConsumption); //previous
    energies.push(this._currentConsumption); //actual
    return energies;
  }
  */

  public baselineConsumption: number = 0; //Observable<number> = new Observable<number>();
  public currentConsumption : number = 0; //Observable<number> = new Observable<number>();
  public baselinePercent: number = 100;

  public getCurrentPercent() { return (this.getCurrentConsumption() / this.getBaselineConsumption()) * 100; }

  public getBaselineRadius() {return ((175 * this.baselinePercent) / 100);}

  //public currentPercent: number = 100;

  public getCurrentRadius() { 
    let value = ((175 * this.getCurrentPercent()) / 100); 
    //minimum radium possible on template
    if(value<130){ value = 130; } //si nos vamos a extremos se requiere 60 porque en html template el radio minimo es a 30 con la resta definida
    //maximum radium possible on template
    if(value>200){ value = 200; }    
    return value;
  }
  
  constructor(private energyService: EnergyService, private api: DataServices) { 
    /*
    //esto ya no es necesario ya que la logica de extraer datos se trajo a calculateEnergyConsumption
    this.cConsumptionSubscription = this.energyService.getEnergyCurrentYear$().subscribe(data => {
      console.log("consumption", "Observable Energy Current value");
      //this.addToCurrentConsumption(data.total);
      this.currentConsumption = data.total;
    });

    this.pConsumptionSubscription = this.energyService.getEnergyPreviousYear$().subscribe(data => {
      console.log("consumption", "Observable Energy Previous Year value");
      //this.addToBaselineConsumption(data.total);
      this.baselineConsumption = data.total;
    });
    */
  }

  ngOnInit(): void {
    console.log(this.consoleLogTag, "OnInit and first calculation");
    //this.addToBaselineConsumption(this.baselineConsumptionConstant);
    this.addToBaselineConsumption(this.energy.baseline);
    this.calculateEnergyConsumption();
    //console.log("consumption", this.building);
    //console.log("consumption", this.calendarSearch);

    /*Debo ahora susbcribir el building y calendarSearch al observable para asegurar que SIEMPRE se esten actualizando (no prioritario) 
    Ahora AQUI debo generar las llamadas para construir los datos usando observables.
    */
   this.csEventsSubscription = this.csEventChild.subscribe(data => {
     console.log(this.consoleLogTag, "Observable CalendarSearch");
     console.log(this.consoleLogTag, data);
     this._calendarSearch = <calendarSearch>data;
     console.log(this.consoleLogTag, this._calendarSearch);
     console.log(this.consoleLogTag, this.calendarSearch);
     this.calculateEnergyConsumption();
    });

    this.buildingEventSubscription = this.buildingEventChild.subscribe(data => {
      console.log(this.consoleLogTag, "Observable Building");
      this._building = <Building>data;
      this.calculateEnergyConsumption();
    });

    /*
    this.cConsumptionSubscription = this.energyService.getEnergyCurrentYear$().subscribe((data:Energy) => {
      console.log("Consumption", "Observable CurrentConsumption triggered");
      this._currentConsumption = data.total;
    }); 
    this.pConsumptionSubscription = this.energyService.getEnergyPreviousYear$().subscribe((data:Energy) => {
      console.log("Consumption", "Observable PreviousConsumption triggered");
      this._baselineConsumption = data.total;
    }); */

    /*
    this.currentConsumption = Observable.create((observer: Observer<number>) => {
      const myValue = this.energyService.getEnergyCurrentYear$().subscribe(data => {
        observer.next(data.total);
      });
      return () => true;
    });
  
    this.baselineConsumption = Observable.create((observer: Observer<number>) => {
      const myValue = this.energyService.getEnergyPreviousYear$().subscribe(data => {
        observer.next(data.total);
      });
      return () => true;
    });
    */

    /*this.currentConsumption = Observable.create( (observer: Observer<number>) => {
      let value = 0;
      const interval = setInterval(() => {
        observer.next(value);
        value++;
      },1000);

      return () => clearInterval(interval);
    });*/
  }

  /*ngOnChanges(changes: SimpleChanges){
    console.log("consumption", "Some changes occur ");
    console.log("consumption", this.building);
    console.log("consumption", this.calendarSearch);
    if(changes['building']){ }
  }*/

  /*async SearchEvent1(search: calendarSearch) {
    console.log("consumption", "New calendar event!");
    console.log("consumption", search);
  }*/

  async calculateEnergyConsumption(){
    console.log(this.consoleLogTag, "Calculating Energy Consumption...");

    //this if logic loads on first time as object has not loaded completely and event emmiter doesnt arrives
    if(this.calendarSearch == undefined){ 
      this.calendarSearch = new calendarSearch();
      //this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate());
    }

    let startDate = this.calendarSearch.minDate.toDateString();
    let endDate = this.calendarSearch.maxDate.toDateString();
    console.log(this.consoleLogTag, "From: "+startDate+" to: "+ endDate);
    console.log(this.consoleLogTag, this.calendarSearch);

    //let startDatePrev = this.energyService.getDateToCompare(this._calendarSearch.minDate, -365).toDateString();
    //let endDatePrev = this.energyService.getDateToCompare(this._calendarSearch.maxDate, -365).toDateString(); 
    //let dateRangePrev = await this.energyService.getDatesBetween(startDatePrev, endDatePrev);    
    //let datepipe: DatePipe = new DatePipe('en-EU');
    //let currentYear = this.getEnergy(Floor, Search, minDate, maxDate, enums.calculationType.currentYear);
    //this.energyCurrent$.next(currentYear);

    //ACTUAL
    let dateRange = this.energyService.getDatesBetween(startDate, endDate);
    console.log(this.consoleLogTag, dateRange);

    //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
    let tmpCalendar: calendarSearch = new calendarSearch();
    tmpCalendar.minDate = new Date(startDate);
    tmpCalendar.maxDate = new Date(endDate);
    tmpCalendar.timeInterval = this.calendarSearch.timeInterval; //possibly memory link need to review later
    tmpCalendar.weekDays = this.calendarSearch.weekDays; //possibly memory link need to review later

    let measure: Measurement = new Measurement(); //recommendation create object with constructor
    measure.CalendarSearch = tmpCalendar; //this.calendarSearch;
    measure.ObjectId = this.building.id; //possibly memory link need to review later
    measure.SensorType = SensorType.Energy;
    measure.MeasurementType = MeasurementType.Sum;
    measure.ObjectType = ObjectType.Building; //redundant if we consider we provide objectI = building.id consider removing from logic in the future
    
    console.log(this.consoleLogTag, measure);

    this.api.getMeasurement(measure).subscribe( //one single call for data
      (data: any) => {
        console.log(this.consoleLogTag, data)
        if(data != undefined){
          this.resetCurrentConsumption();
          this.addToCurrentConsumption(data.sum);

          this.resetBaseLineConsumption();
          this.addToBaselineConsumption((this.energy.baseline * tmpCalendar.getNumberOfDaysConsideredInCalculus()));
        }
      }
    );
    /*
    //CODIGO PENSADO EN LLAMAR UN PERIODO ANTERIOR PARA RESTAR PERO NO SE REQUIERE POSIBLEMENTE LO ELIMINEMOS
    console.log("consumption", this.calendarSearch);
    startDate = this.energyService.getDateToCompare(new Date(startDate), -365).toDateString();
    endDate = this.energyService.getDateToCompare(new Date(endDate), -365).toDateString(); 
    let prevCalendarSearch: calendarSearch = this.calendarSearch;
    prevCalendarSearch.setMinDate(new Date(startDate));
    prevCalendarSearch.setMaxDate(new Date(endDate));
    console.log("consumption", prevCalendarSearch);
    console.log("consumption", this.calendarSearch);
    */

    //CALCULO DE DATOS EXTRAYENDO DATOS POR PISO PARA LUEGO SUMARLOS, EXTRAYENDO DATOS TANTO DE PERIODO ACTUAL
    //COMO DE PERIODO ANTERIOR
    //ESTE ENFOQUE PARECE QUE TAMPOCO ES NECESARIO POR LO QUE SE ELIMINARA POSIBLEMENTE


    /*
    //ACTUAL 
    this.building.floors.forEach( (floor: Floor) => {
      console.log("consumption", "Iterating on floor: " + floor.id);
      console.log("consumption", dateRange); //checar si el get measurements puede obtener de varios dias segun yo si. CHECAR CHECAR CHECAR!!!!
      dateRange.forEach( (date:Date) => {
        console.log("consumption", "Iterating on daterange: " + date);
        let measurement: Measurement = new Measurement();

        measurement.CalendarSearch = this.calendarSearch;
        measurement.ObjectId = this.building.id; //floor.id;
        measurement.SensorType = SensorType.Energy;
        measurement.MeasurementType = MeasurementType.Sum;
        measurement.ObjectType = ObjectType.Building;
        //We request ENERGY data from each Floor 

        console.log("consumption", measurement);
        this.api.getMeasurement(measurement).subscribe( //NO DEUEVLE DATOS POR EL TENANT ID....CHECAR DE DONDE SE SACA
          (data: any) => {
            console.log("consumption", "Getting measures");
            if(data != undefined) {
              console.log("consumption", data);
              this.addToBaselineConsumption(10); //data.sum
              let formatedDate = datepipe.transform(date, 'MMMM d') ?? "";
              this._baselineConsumption.timeLine.push({date: formatedDate, value: data.sum})
            }
          },
          err => {
            console.log("consumption", err);
          });
      });
    });
    */
     
    /*
    //PREVIOUS
    //let tmpDate:Date = new Date();
    //tmpDate.setDate(this._calendarSearch.minDate.getDate()-365);
    //startDate = tmpDate.toDateString();
    startDate = this.energyService.getDateToCompare(this._calendarSearch.minDate, -365).toDateString();
    //tmpDate.setDate(this._calendarSearch.maxDate.getDate()-365);
    //endDate = tmpDate.toDateString();   
    endDate = this.energyService.getDateToCompare(this._calendarSearch.maxDate, -365).toDateString(); 
    let dateRangePrev = await this.energyService.getDatesBetween(startDate, endDate);

    this.building.floors.forEach(async (floor: Floor) => {
      dateRangePrev.forEach( (date:Date) => {
        let measurement: Measurement = new Measurement();

        measurement.CalendarSearch = this.calendarSearch;
        measurement.ObjectId = floor.id;
        measurement.SensorType = SensorType.Energy;
        measurement.MeasurementType = MeasurementType.Sum;
        measurement.ObjectType = ObjectType.Area;

        this.api.getMeasurement(measurement).subscribe(
          (data: any) => {
            if(data != undefined) {
              this.addToCurrentConsumption(12); //data.sum va aqui
              let formatedDate = datepipe.transform(date, 'MMMM d') ?? "";
              this._currentConsumption.timeLine.push({date: formatedDate, value: data.sum})
            }
          },
          err => {
            console.log(err);
          });          

      });
    });
    */
  }

}
