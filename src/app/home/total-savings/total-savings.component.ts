import { Component, Input, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Building } from 'src/app/models/building.model';
import { calendarSearch } from 'src/app/models/calendarSearch.model';
import { Measurement } from 'src/app/models/measurements/measurement';
import { MeasurementType } from 'src/app/models/measurements/measurementType.model';
import { ObjectType } from 'src/app/models/measurements/objectType.model';
import { SensorType } from 'src/app/models/measurements/sensorType.model';
import { DataServices } from 'src/app/services/data.service';
import { EnergyService } from 'src/app/services/energy.service';

@Component({
  selector: 'app-total-savings',
  templateUrl: './total-savings.component.html',
  styleUrls: ['./total-savings.component.css']
})
export class TotalSavingsComponent implements OnInit {

  private consoleLogTag: string = "totalSavings";

  public baselineConsumptionConstant = 1755.36; //in KwH
  public energySavings: number = 0;
  public isEnergySavings: boolean = false;

  private _building !: Building;
  @Input() set building(value:Building) { this._building = value }
  get building() { return this._building }

  private _calendarSearch !: calendarSearch
  @Input() set calendarSearch(value:calendarSearch) {this._calendarSearch = value }
  get calendarSearch() { return this._calendarSearch }
  
  @Input() csEventChild !: Observable<calendarSearch>;
  private csEventsSubscription !: Subscription;

  @Input() buildingEventChild !: Observable<Building>;
  private buildingEventSubscription !: Subscription;

  constructor(private energyService: EnergyService, private api: DataServices) { 
    this._calendarSearch = new calendarSearch();
  }
  
  setEnergySavings(value:number) {
    this.energySavings = value;
  }

  getEnergySavings() : number {
    return this.energySavings;
  }

  ngOnInit(): void {
    console.log(this.consoleLogTag, "OnInit and first calculation");
    this.setEnergySavings(0);//reset Savings to zero.
    this.calculateEnergySavings();

    this.csEventsSubscription = this.csEventChild.subscribe(data => {
      console.log(this.consoleLogTag, "Observable CalendarSearch");
      console.log(this.consoleLogTag, data);
      this._calendarSearch = <calendarSearch>data; //debemos desvincularlo creando un objeto nuevo
      console.log(this.consoleLogTag, this._calendarSearch);
      console.log(this.consoleLogTag, this.calendarSearch);
      this.calculateEnergySavings();
     });

     this.buildingEventSubscription = this.buildingEventChild.subscribe(data => {
       console.log(this.consoleLogTag, "Observable Building");
       this._building = <Building>data;
       this.calculateEnergySavings();
     });
  }

  calculateEnergySavings() {  
    console.log(this.consoleLogTag, "Calculating Energy Consumption...");

    //this if loads on first time as object has not loaded completely and event emmiter doesnt arrives
    if(this.calendarSearch == undefined){ 
      this.calendarSearch = new calendarSearch();
      this._calendarSearch.minDate.setDate(this._calendarSearch.minDate.getDate()); //esto permite jalar los datos del dia anterior
    }

    let startDate = this.calendarSearch.minDate.toDateString();
    let endDate = this.calendarSearch.maxDate.toDateString();
    console.log(this.consoleLogTag, "From: "+startDate+" to: "+ endDate);
    console.log(this.consoleLogTag, this.calendarSearch);

    let dateRange = this.energyService.getDatesBetween(startDate, endDate);
    console.log(this.consoleLogTag, dateRange);

    //creamos variable temporal para desvincular objeto a nivel memoria y evitar conflictos con otras mini cards
    let tmpCalendar: calendarSearch = new calendarSearch();
    tmpCalendar.minDate = new Date(startDate);
    tmpCalendar.maxDate = new Date(endDate);
    tmpCalendar.timeInterval = this.calendarSearch.timeInterval;
    tmpCalendar.weekDays = this.calendarSearch.weekDays;

    let measure: Measurement = new Measurement();
    measure.CalendarSearch = tmpCalendar; //this.calendarSearch;
    measure.ObjectId = this.building.id;
    measure.SensorType = SensorType.Energy;
    measure.MeasurementType = MeasurementType.Sum;
    measure.ObjectType = ObjectType.Building; //redundant if we consider we provide objectI = building.id consider removing from logic in the future
    console.log(this.consoleLogTag, measure);

    this.api.getMeasurement(measure).subscribe( 
      (data: any) => {
        console.log(this.consoleLogTag, data)
        if(data != undefined){
          let tmpEnergySavings =  (this.baselineConsumptionConstant * tmpCalendar.getNumberOfDaysConsideredInCalculus()) - (data.sum); //we convert data from watts to KwH by division to 1,000
          this.setEnergySavings(tmpEnergySavings); //to present data in positive numbers and not in negative (update now we maintain in positive)
          this.isEnergySavings = true; //we allow the data to be presented on screen
        }
      }
    );    

  }

  getSavingsInKilowatts(): number{
    return this.getEnergySavings();
  }

  getSavingsInEuros(): number {
    return this.getEnergySavings() * 0.1;
  }

  getSavingsInCO2(): number {
    return (this.getEnergySavings() * 0.640) / 1000;
  }

  getSavingsInTrees(): number {
    return this.getSavingsInCO2() / 0.06;
  }

  getSavingsInSmartphones() {
    return this.getSavingsInCO2() / 0.00000822;
  }

}
