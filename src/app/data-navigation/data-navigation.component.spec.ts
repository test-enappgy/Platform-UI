import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DataNavigationComponent } from './data-navigation.component';

describe('DataNavigationComponent', () => {
  let component: DataNavigationComponent;
  let fixture: ComponentFixture<DataNavigationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DataNavigationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
