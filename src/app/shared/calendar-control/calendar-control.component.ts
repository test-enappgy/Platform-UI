import { Component, Output, Input, EventEmitter, OnInit } from '@angular/core';
import {calendarSearch} from 'src/app/models/calendarSearch.model'
import { DatePipe } from '@angular/common';
import moment from 'moment';


@Component({
  selector: 'app-calendar-control',
  templateUrl: './calendar-control.component.html',
  styleUrls: ['./calendar-control.component.css']
})

export class CalendarControlComponent implements OnInit {

  @Input() calendarShortcuts!: boolean;
  @Input() calendarDatepicker!: boolean;
  showCalendarShortcuts: boolean = false;
  showCalendarDatepicker: boolean = false;

  @Input() allowWeekDays:boolean = true;
  @Input() allowTime:boolean = true;
  @Input() allowDateRange:boolean = true;

  @Output() searchClick: EventEmitter<calendarSearch> = new EventEmitter();

  @Input() allowTimeRange:boolean = true;

  public buttonTimeRangeSelected: number = 0; //to identify which button is being pressed

  public searchTypes: string[] = new Array("Last Week","Last Month","Three Months","Six Months", "One Year", "Custom",);

  public focusedDate: Date = new Date();
  public today: Date =  new Date(moment().toDate());
  public dateLastWeek: Date = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate() - 7)
  public minDate: Date = new Date(moment().toDate());
  public maxDate: Date = new Date(moment().toDate());

  public range = {
    start: new Date(2018, 10, 10),
    end: new Date(2018, 10, 12),
  };

  public WeekDays: ReadonlyArray<string> = new Array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
  public selected :any;
  public selectedWeekDays: string[] = ["Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

  public minSlider: number = 0;
  public maxSlider: number = 24;
  public largeStep: number = 2;
  public valueRange: [number, number] = [0, 0];
  private times: ReadonlyArray<string> = new Array(
    "0:00", "1:00", "2:00", "3:00", "4:00", "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00", "24:00"
    );

  public showWeekDays: boolean = false;
  public showTime: boolean = false;
  toggleCard(variable: string): void {
    if (variable === 'showWeekDays') {
      this.showTime = false;
      this.showWeekDays = !this.showWeekDays;
    }
    else if (variable === 'showTime') {
      this.showWeekDays = false;
      this.showTime = !this.showTime;
    }
  }

  public calendarSelected: calendarSearch = new calendarSearch();
  private DatePipe: DatePipe = new DatePipe('en-EU')

  constructor() {
    console.log("calendar", this.calendarSelected);
    console.log("calendar", this.calendarDatepicker);
    console.log("calendar", this.calendarShortcuts);
   }
  
  
  ngOnInit () {
    this.showCalendarDatepicker = this.calendarDatepicker;
    this.showCalendarShortcuts = this.calendarShortcuts;

    console.log("calendar", this.calendarDatepicker);
    console.log("calendar", this.calendarShortcuts);
    console.log("calendar", this.calendarSelected);
    console.log("calendar", this.minDate);
    //this.calendarSelected.minDate = this.minDate;
    console.log("calendar", this.calendarSelected.minDate);
    //this.calendarSelected.minDate.setDate(this.calendarSelected.minDate.getDate() - 1);
    //this.calendarSelected.maxDate = this.maxDate;
    console.log("calendar", this.calendarSelected);
    let currentDay = this.DatePipe.transform(this.maxDate, 'EEEE') ?? "";
    //this.calendarSelected.weekDays = new Array(currentDay);
    //this.daysOfWeeSelection(currentDay);
    this.calendarSelected.timeInterval = ["00:00:00", "23:59:59"];
    this.Search(true);

    console.log("calendar", this.calendarSelected);
    //this.searchClick.emit(this.calendarSelected);    
  }

  searchType(value:string):void {
      if(!value === undefined){
          if(value == "Custom"){
          }
      }
  }

  daysOfWeeSelection(item: any): void {
   this.selected = (this.selected === item ? null : item); 
      if(this.selectedWeekDays.some(x => x == item)){
        this.selectedWeekDays = this.selectedWeekDays.filter(x=> x !== item);
      }
      else{
          this.selectedWeekDays.push(item);
      }
  }

  isActive(item:any) {
    let tmpValue = '';
    //console.log("calendar", this.selectedWeekDays.includes(item) ? true : false);
    //console.log("calendar", item);
    return this.selectedWeekDays.includes(item) ? true : false;
  };

  public title = (value: any) => { return this.times[value]; };

  Search(isInit: boolean) {
    console.log("calendar", "Generating new batch for calendarData...");
    if(isInit){
      console.log("calendar", this.calendarSelected);
      this.searchClick.emit(this.calendarSelected);
    }
    else{
      //this.calendarSelected.minDate = this.minDate;
      this.calendarSelected.setMinDate(this.minDate);
      //this.calendarSelected.maxDate = this.maxDate;
      this.calendarSelected.setMaxDate(this.maxDate);
      this.calendarSelected.weekDays = this.selectedWeekDays;
      this.calendarSelected.timeInterval[0] = this.setTimeValues(this.valueRange[0], "startDate");
      //if(this.valueRange[1]== 24){ this.valueRange[1]= 0} //considering that the API call requires min:0,max:0 to retrieve whole day
      this.calendarSelected.timeInterval[1] = this.setTimeValues(this.valueRange[1], "endDate");
      console.log("calendar", this.calendarSelected);
      this.searchClick.emit(this.calendarSelected);
    }
  }
  
  //hay que rehacer esta funcion y fusionarla con Settime Frame, no hay 24 hs en un dia sino hasta 23:59
  setTimeValues(time: number, timePoint: string) {
    let result = "";

    time == 24 ? time = 23 : time = time; //si la hora es 24 la reajustamos a 23 ya que las hs van de 0 a 23:59

    result = time.toLocaleString('en-US', {
      minimumIntegerDigits: 2,
      useGrouping: false
    });

    timePoint == "startDate" ? result = result + ":00:00" : result = result + ":59:59";

    /*switch(time){
      case 0:
        timePoint == "startData" ? result = "00:00:00" : result = "00:59:59";
        break;
      case 1:
        timePoint == "startData" ? result = "01:00:00" : result = "01:59:59";
        break;
        case 2:
          result = "02:00:00"
          break;
          case 3:
        result = "03:00:00"
        break;
        case 4:
        result = "04:00:00"
        break;
        case 5:
        result = "05:00:00"
        break;
        case 6:
        result = "06:00:00"
        break;
        case 7:
        result = "07:00:00"
        break;
        case 8:
        result = "08:00:00"
        break;
        case 9:
        result = "09:00:00"
        break;
        case 10:
        result = "10:00:00"
        break;
        case 11:
        result = "11:00:00"
        break;
        case 12:
        result = "12:00:00"
        break;
        case 13:
        result = "13:00:00"
        break;
        case 14:
        result = "14:00:00"
        break;
        case 15:
        result = "15:00:00"
        break;
        case 16:
        result = "16:00:00"
        break;
        case 17:
        result = "17:00:00"
        break;
        case 18:
        result = "18:00:00"
        break;
        case 19:
        result = "19:00:00"
        break;
        case 20:
        result = "20:00:00"
        break;
        case 21:
        result = "21:00:00"
        break;
        case 22:
        result = "22:00:00"
        break;
        case 23:
        result = "23:00:00"
        break;
        case 24:
        result = "00:00:00" //to indicate to the API the whole day the API request 0,0 on the range
        break;
    } */
    console.log("calendar", result);
    return result;
  }

  setTimeFrame(days:number) {
    console.log("calendar", "Generating new data for calendar")
    this.buttonTimeRangeSelected = days; //this identifies which button we have pressed

    this.selectedWeekDays = ["Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
    //this.minDate = this.today;
    console.log("calendar", "Range option selected: " + days);

    //default
    //this.maxDate.setDate(new Date(this.today).getDate()+days);
    this.minDate = new Date();
    this.minDate.setDate(this.minDate.getDate() -  days);
    let tmpDate1 = new Date();
    let tmpDate2 = new Date();
    console.log("calendar", tmpDate1);
    console.log("calendar", tmpDate2);
    //options
    switch(days){
      case 0: //current day
        this.minDate = tmpDate1;
        break;
      case 1: //last day
        this.minDate.setDate(tmpDate1.getDate() - days);
        //this.minDate.setDate(this.minDate.getDate() -  days);
        this.maxDate.setDate(tmpDate1.getDate() - days);
        //this.maxDate.setDate(this.maxDate.getDate() -  days);
        break;
      case 6: //last week
        tmpDate1.setDate(tmpDate1.getDate() - (tmpDate1.getDay() + 6) % 7); //set to Monday of this week
        console.log("calendar", tmpDate1);
        tmpDate1.setDate(tmpDate1.getDate() - 7); //set to previous Monday
        console.log("calendar", tmpDate1);
        this.minDate = new Date(tmpDate1);
        //this.minDate = new Date(tmpDate1.getFullYear(), tmpDate1.getMonth(), tmpDate1.getDate() - 1); //create a new date of the day before
        //this.minDate.setDate(this.minDate.getDate() -  days);
        this.maxDate.setDate(tmpDate1.getDate() + days); //set Sunday
        //this.maxDate.setDate(this.maxDate.getDate() -  days);
        break;
      case 30: //last month
        const firstDay = 1; 
        this.minDate = new Date(tmpDate1.getFullYear(), tmpDate1.getMonth() - 1, firstDay); //first day of previous month
        this.maxDate = new Date(tmpDate1.getFullYear(), tmpDate1.getMonth(), 0); //last day of the month
        break;
    }

    console.log("calendar", this.today);
    console.log("calendar", this.minDate);
    console.log("calendar", this.maxDate); //currentDate
    console.log("calendar", this.minDate);
    
    this.calendarSelected = new calendarSearch();
    //this.calendarSelected.timeInterval = ["00:00:01", "23:59:59"];
    this.calendarSelected.weekDays = this.selectedWeekDays;
    this.calendarSelected.setMinDate(this.minDate);
    this.calendarSelected.setMaxDate(this.maxDate);
    console.log("calendar", this.calendarSelected);
    this.searchClick.emit(this.calendarSelected);
  }

}