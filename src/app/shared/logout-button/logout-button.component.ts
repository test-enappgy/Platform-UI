import { Component, Inject, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';
import { DOCUMENT } from '@angular/common';
import { Offset } from "@progress/kendo-angular-popup";

@Component({
  selector: 'app-logout-button',
  templateUrl: './logout-button.component.html',
  styleUrls: ['./logout-button.component.css']
})

export class LogoutButtonComponent implements OnInit {
  public profileJson:string = ''
  public show = false;
  public onToggle(): void {
    this.show = !this.show;
  }
  public margin = { horizontal: 0, vertical: 16 };
  constructor(
    public auth: AuthService,
    @Inject(DOCUMENT) private doc: Document
  ) { }

  ngOnInit(): void {
    this.auth.user$.subscribe(
      (profile) => {
        this.profileJson = JSON.stringify(profile, null, 2);
        //console.log(this.profileJson);
        //console.log(profile?.id);
      }      
    );
  }

  logout(): void {
    this.auth.logout({ returnTo: this.doc.location.origin });
  }

}
