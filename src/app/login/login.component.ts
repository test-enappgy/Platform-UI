import { Component, ViewChild, ViewEncapsulation, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl } from "@angular/forms";
import { TextBoxComponent } from "@progress/kendo-angular-inputs";

@Component({
  selector: 'app-login',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @ViewChild("password") public textbox!: TextBoxComponent;

  public ngAfterViewInit(): void {
    this.textbox.input.nativeElement.type = "password";
  }

  public toggleVisibility(): void {
    const inputEl = this.textbox.input.nativeElement;
    inputEl.type = inputEl.type === "password" ? "text" : "password";
  }

  public form: UntypedFormGroup = new UntypedFormGroup({
    username: new UntypedFormControl(),
    password: new UntypedFormControl(),
    loggedin: new UntypedFormControl(),
  });

  public login(): void {
    this.form.markAllAsTouched();
  }

  public clearForm(): void {
    this.form.reset();
  }
  constructor() { }

  ngOnInit(): void {
  }

}
