import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment as env } from '../../../environments/environment';

interface Message {
  message: string;
}

@Component({
  selector: 'app-external-api',
  templateUrl: './external-api.component.html',
})
export class ExternalApiComponent implements OnInit {
  message: string = '';

  constructor(private http: HttpClient) {}

  ngOnInit(): void {}
  callApi(): void {
    alert('Public message');
    this.http
      .get(env.enappgyData.serverUrl)
      .subscribe((result) => {
        this.message = String(result);
      });
  }

  callSecureApi(): void {
    alert('Protected message');
    this.http
      .get(env.enappgyData.protectedMessage)
      .subscribe((result) => {
        this.message = String(result)
      });
  }
}