import { AccountService } from '../services/account.service';
import { DataServices } from '../services/data.service';
import { Databucket } from './databucket.model';
import { DataPool } from './dataPool.model';
import Dictionary from './dictionary.model';
import { Floor } from './floor.model';

export class Building {
  id: string;
  description: string;
  databuckets: Databucket[];
  dataPools = new Dictionary<Databucket[]>();
  floors: Floor[];
  
  floorPlanId:string = 'NaN';
  archilogicId: string = 'NaN';

  constructor(jsonObject: any, private api?: DataServices) {
    this.id = jsonObject.id;

    if(typeof jsonObject.description != 'undefined'){
      this.description = jsonObject.description;
    } else {
      this.description = 'NaN';
    }
    
    this.databuckets = [];  
    console.log("Building", jsonObject)
    if(typeof jsonObject.dataBucket != 'undefined'){
      let databucket = new Databucket(jsonObject.dataBucket);
      this.databuckets.push(databucket);
    }  
    //this.dataPools = [];

    if(typeof jsonObject.floorPlanId != 'undefined'){
      this.floorPlanId = jsonObject.floorPlanId;
      this.archilogicId = jsonObject.floorPlanId;
    }

    this.floors = [];
    if(typeof jsonObject.floors != 'undefined' && jsonObject.floors != null){
      this.setFloors(jsonObject.floors);
    }
  }  

  builder(jsonObject: any){
    this.id = jsonObject.id;
    this.description = jsonObject.description;    
    //this.setFloors(jsonObject.floors);
  }

  setFloors(jsonObject:any){
    this.floors = [];
    if(jsonObject != null){
        for(let i=0; i<jsonObject.length; i++){
            let floor = new Floor(jsonObject[i]);
            this.floors.push(floor);
        }
        return true;
    } else {
        return false;
    }

}
  /*
  setDatabucket(databucket: Databucket) {
    this.databuckets = [];
    this.databuckets.push(databucket);
    return true;
  }

  getDatabucket() {
    return this.databuckets;
  }
  */
  setDatabuckets(jsonObject:any){

    if(jsonObject != null){
        for(let i=0; i< jsonObject.length; i++){
            let databucket = new Databucket(jsonObject[i]);
            this.databuckets.push(databucket);
        }
        return true;
    } else {
        return false;
    }
  }

  setDataPool(sensorType:string, jsonObject:any){
    let databuckets: Databucket[];
    databuckets = [];

    if(jsonObject != null){      
      for(let i=0; i< jsonObject; i++){                                
          let databucket = new Databucket(jsonObject[i]);
          databuckets.push(databucket);
      }
      if(this.dataPools.containsKey(sensorType)){
        this.dataPools.removeItem(sensorType);
      }
      this.dataPools.add(sensorType, databuckets);
      
      /*
        let dataPool = new DataPool(sensorType, description);
        dataPool.setDatabuckets(jsonObject);
        this.dataPools.push(dataPool);
      */
        return true;
    } else {
        return false;
    }
  }

  getDataPool(){
      return this.dataPools;
  }     

  getDatabucketTotal(typeOfValue: string) {
    let total = 0;
    for (let i = 0; i < this.databuckets.length; i++) {
      total += this.databuckets[i].getValue(typeOfValue);
    }
    return total;
  }

  getTrimDatabucketTotal(sensorType: string, typeOfValue: string,fromIndex: number,tillIndex: number) {
    let total = 0;
    let databuckets: Databucket[];
    databuckets = this.dataPools.getItem(sensorType);
    if(tillIndex == 0){
      tillIndex = databuckets.length;
    }
    for(let i=fromIndex; i<tillIndex; i++){
      total += databuckets[i].getValue(typeOfValue);
    }   
    //this.dataTotal= total;
    return total;
  }

  getTotal(){
    return 99;
  }
}
