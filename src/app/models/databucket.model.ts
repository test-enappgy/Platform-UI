export class Databucket {
  from: string;
  till: string;
  avg: number;
  min: number;
  max: number;
  sum: number;

  constructor(jsonObject: any) {
    this.from = jsonObject.from;
    this.till = jsonObject.till;
    this.avg = jsonObject.avg;
    this.min = jsonObject.min;
    this.max = jsonObject.max;
    this.sum = jsonObject.sum;
  }

  getValue(typeOfValue: string) {
    let value: any;
    value = 0;

    switch (typeOfValue) {
      case 'from':
        value = this.from;
        break;
      case 'till':
        value = this.till;
        break;
      case 'avg':
        value = Number(this.avg);
        break;
      case 'min':
        value = Number(this.min);
        break;
      case 'max':
        value = Number(this.max);
        break;
      case 'sum':
        value = Number(this.sum);
        break;
    }
    return value;
  }
}
