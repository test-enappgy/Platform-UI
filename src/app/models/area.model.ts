import { Channel } from "./channel.model";
import { Coordinates } from "./coordinates.model";
import { Databucket } from "./databucket.model";
import { DataPool } from "./dataPool.model";
import Dictionary from "./dictionary.model";
import { Energy } from "./energy.model";
import { SensorType } from './measurements/sensorType.model';

export class Area{
    id:string;
    description:string = 'NaN';
    coordinates: Coordinates;
    channels: Channel[];
    databuckets: Databucket[]; //for internal use to assign to dataPool
    dataPools = new Dictionary<Databucket[]>();
    dataTotal:number = 0;
    archilogicId: string = 'NaN';
    floorPlanId: string = 'NaN';
    spaceId: string = 'NaN';
    energy!: Energy;
//TODO: Properly assign floorPlanId to area.
    constructor(jsonObject:any){
        this.id = jsonObject.id;
        if(typeof jsonObject.description != 'undefined'){
            this.description = jsonObject.description;
        }
        this.coordinates = jsonObject.coordinates;

        this.databuckets = [];  
        console.log("Area", jsonObject)
        if(typeof jsonObject.dataBucket != 'undefined'){
          let databucket = new Databucket(jsonObject.dataBucket);
          this.databuckets.push(databucket);
        }

        if(typeof jsonObject.floorPlanId != 'undefined'){
          this.floorPlanId = jsonObject.floorPlanId;
          this.archilogicId = jsonObject.floorPlanId;
        }
        if(typeof jsonObject.spaceId != 'undefined'){
            this.spaceId = jsonObject.spaceId
        }
        this.channels = [];
        if(typeof jsonObject.channels != 'undefined' && jsonObject.channels != null){
            this.setChannels(jsonObject.channels);
        }
    }

    builder(jsonObject: any){
        this.id = jsonObject.id;
        this.description = jsonObject.description;    
        this.coordinates = jsonObject.coordinates;
        this.setChannels(jsonObject.channels);
    }

    setDataPool(sensorType:string, jsonObject:any){
        let databuckets: Databucket[];
        databuckets = [];
        if(jsonObject != null){      
          for(let i=0; i< jsonObject.length; i++){                                
              let databucket = new Databucket(jsonObject[i]);
              databuckets.push(databucket);
          }
          this.dataPools.add(sensorType, databuckets);
            return true;
        } else {
            return false;
        }
    }

    getDataPool(){
        return this.dataPools;
    }

    setChannels(jsonObject:any){
        this.channels = [];
        if(jsonObject != null){
            for(let i=0; i<jsonObject.length; i++){
                let channel = new Channel(jsonObject[i]);
                this.channels.push(channel);
            }
            return true;
        } else {
            return false;
        }
    }

    sumDatabuckets(typeOfValue:string){
        let value: number;
        value = 0;
        for(let i=0; i<this.databuckets.length; i++){
            value += this.databuckets[i].getValue(typeOfValue);
        }
        return value;
    }

    //TODO: Validate calculations and returned value
    getTrimDatabucketTotal(sensorType: string, typeOfValue: string, fromIndex: number, tillIndex: number) {
        let total = 0;
        let databuckets: Databucket[];
        databuckets = this.dataPools.getItem(sensorType.toString());
        if(tillIndex == 0){
            tillIndex = databuckets.length;
          }
        for (let i = fromIndex; i < tillIndex; i++) {
          total += databuckets[i].getValue(typeOfValue);
        }
        total = total/(tillIndex-fromIndex);
        this.dataTotal = total;
        return total;
    }
}