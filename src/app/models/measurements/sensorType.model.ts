export enum SensorType {
    Energy = 0, Occupancy = 1, LightLevel = 2, Sound = 3, Temperature = 4, CO2 = 5, Humidity = 6
}