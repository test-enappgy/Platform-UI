/* global mapboxgl */
import { svgToImage } from './svg-to-png.js'

export async function loadFloor(floorId, publishableToken) {
  const map = new mapboxgl.Map({
    container: 'map', // container id
    center: [0, 0],
    zoom: 19
  })

  const data = await Promise.all([
    loadFloorPlanImage(floorId, publishableToken),
    loadGeoJson(floorId, 'space', publishableToken),
    loadGeoJson(floorId, 'asset', publishableToken)
  ])
  const [imageData, spaceFeatures, assetFeatures] = data

  const workSpaceIds = spaceFeatures.features
    .filter((spaceFeature) => spaceFeature.properties.program === 'work')
    .map((spaceFeature) => spaceFeature.id)

  // check space taxonomy for details
  // https://developers.archilogic.com/space-api/v2/space-taxonomy.html#hierarchy
  spaceFeatures.features = spaceFeatures.features.filter((spaceFeature) =>
    ['meet', 'socialize', 'care'].includes(spaceFeature.properties.program)
  )

  // check product taxonomy for details
  // https://developers.archilogic.com/space-api/v2/product-taxonomy.html#hierarchy
  assetFeatures.features = assetFeatures.features.filter((assetFeature) => {
    const isWorkSpace = workSpaceIds.includes(assetFeature.resourceRelations.spaces[0])
    const isDeskOrChair =
      assetFeature.properties.subCategories.includes('desk') ||
      assetFeature.properties.subCategories.includes('taskChair')
    return isWorkSpace && isDeskOrChair
  })
  // draw Geo JSON polygon contours of the spaces
  await addFloorPlanImage(imageData, map)
  addGeoJson(spaceFeatures, map, 'space')
  addGeoJson(assetFeatures, map, 'asset')
  removeOverlay()
}

// Load svg floor plan from Space API
function loadFloorPlanImage(floorId, publishableToken) {
  return fetch(
    `https://api.archilogic.com/v2/floor/${floorId}/2d-image?pubtoken=${publishableToken}`,
    {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' }
    }
  ).then((res) => res.json())
}

// Query resources from floorId and add their Geo JSON polygon contours
function loadGeoJson(floorId, resourceType = 'space', publishableToken) {
  return fetch(
    `https://api.archilogic.com/v2/${resourceType}?floorId=${floorId}&geometry=true&pubtoken=${publishableToken}`
  ).then((res) => res.json())
}

async function addFloorPlanImage(imageData, map) {
  const { imageUrl, latLngBounds: latLng } = imageData

  // mapboxGl expects GeoJson like lngLat coordinates
  const coordinates = [
    [latLng[0][1], latLng[0][0]],
    [latLng[1][1], latLng[0][0]],
    [latLng[1][1], latLng[1][0]],
    [latLng[0][1], latLng[1][0]]
  ]
  // mapboxGl can not load svg directly
  const imageDataUrl = await svgToImage(imageUrl, 4096)

  // add as image overlay
  // add to map
  map.addSource('floorPlan', {
    type: 'image',
    url: imageDataUrl,
    coordinates
  })
  map.addLayer({
    id: 'floorPlan',
    source: 'floorPlan',
    type: 'raster',
    paint: { 'raster-opacity': 1 }
  })
}

function addGeoJson(geoJson, map, id) {
  // Add a data source containing GeoJSON data.
  map.addSource(id, {
    type: 'geojson',
    data: geoJson
  })

  // Add a new layer to visualize the polygon.
  map.addLayer({
    id: `${id}-fill`,
    type: 'fill',
    source: id, // reference the data source
    layout: {},
    paint: {
      'fill-color': id === 'space' ? '#0080ff' : '#EB984E', // blue color fill
      'fill-opacity': 0.3
    }
  })

  map.on('click', `${id}-fill`, function (e) {
    var features = map.queryRenderedFeatures(e.point)
    new mapboxgl.Popup().setLngLat(e.lngLat).setHTML(featureInfo(features[0])).addTo(map)
  })

  // Add a black outline around the polygon.
  map.addLayer({
    id: `${id}-outline`,
    type: 'line',
    source: id,
    layout: {},
    paint: {
      'line-color': '#000',
      'line-width': 1
    }
  })
}

function removeOverlay() {
  document.getElementById('overlay').style.visibility = 'hidden'
}

function featureInfo(ft) {
  // mapbox gl removes any non standard fields outside of properties
  const isSpace = !!ft.properties.usage
  const isAsset = !!ft.properties.name
  let info = `type: ${isSpace ? 'Space' : 'Asset'}<br>`
  if (isSpace)
    info += `usage: ${camelToTitle(ft.properties.usage)}<br>
  program: ${camelToTitle(ft.properties.program)}<br>
  asset count: ${ft.resourceRelations?.assets?.length || 0}`
  else if (isAsset)
    info += `name: ${ft.properties.name}<br>
  category: ${camelToTitle(JSON.parse(ft.properties.categories)[0])}<br>
  sub category: ${camelToTitle(JSON.parse(ft.properties.subCategories)[0])}<br>`
  return info
}

function camelToTitle(key) {
  return key
    .replace(/([A-Z])/g, (match) => ` ${match.toLowerCase()}`)
    .replace(/^./, (match) => match.toUpperCase())
    .trim()
}
