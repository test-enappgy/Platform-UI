#Stage 1: Compile and Build Angular codebase
FROM node:16.13.0 as builder
LABEL enappgyui.version="0.1.0" enappgyui.release-date="2021-11-30"
LABEL Author = "Daniel Velazquez"

ENV NODE_ENV=production
WORKDIR /enappgy
RUN npm cache clean --force
COPY . /enappgy/
COPY package.json package-lock.json /enappgy/
RUN npm install
RUN npm install -dev exit-on-epipe
RUN npm run build --prod
#Stage 2: Serve app with nginx server
FROM nginx:stable
RUN rm -rf /usr/share/nginx/html/*
COPY --from=builder ./enappgy/dist/enappgy /usr/share/nginx/html
ENTRYPOINT ["nginx", "-g", "daemon off;"]
EXPOSE 80
